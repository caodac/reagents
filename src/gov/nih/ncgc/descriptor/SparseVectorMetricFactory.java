// $Id$

package gov.nih.ncgc.descriptor;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class SparseVectorMetricFactory {
    private SparseVectorMetricFactory () {
    }

    public static class Cosine implements SparseVectorMetric {
	public double compute (Map<String, Integer> v1, 
			       Map<String, Integer> v2) {
	    return cosine (v1, v2);
	}
    }

    public static class Robust implements SparseVectorMetric {
	public double compute (Map<String, Integer> v1,
			       Map<String, Integer> v2) {
	    return robust (v1, v2);
	}
    }

    public static class Jaccard implements SparseVectorMetric {
	public double compute (Map<String, Integer> v1,
			       Map<String, Integer> v2) {
	    return jaccard (v1, v2);
	}
    }

    public static class Dice implements SparseVectorMetric {
	public double compute (Map<String, Integer> v1,
			       Map<String, Integer> v2) {
	    return dice (v1, v2);
	}
    }

    public static class Tanimoto implements SparseVectorMetric {
	public double compute (Map<String, Integer> v1,
			       Map<String, Integer> v2) {
	    return tanimoto (v1, v2);
	}
    }

    public static class Lingo implements SparseVectorMetric {
	public double compute (Map<String, Integer> v1,
			       Map<String, Integer> v2) {
	    return lingo (v1, v2);
	}
    }

    public static class Sim implements SparseVectorMetric {
	public double compute (Map<String, Integer> v1,
			       Map<String, Integer> v2) {
	    return sim (v1, v2);
	}
    }

    public static double cosine (Map<String, Integer> v1, 
				 Map<String, Integer> v2) {
	Set<String> common = new TreeSet<String>(v1.keySet());
	common.retainAll(v2.keySet());

	double xy = 0., x = 0., y = 0.;
	for (String s : common) {
	    int c1 = v1.get(s), c2 = v2.get(s);
	    xy += c1*c2;
	}

	for (Integer c : v1.values()) {
	    x += c*c;
	}
	for (Integer c : v2.values()) {
	    y += c*c;
	}

	return xy / Math.sqrt(x*y);
    }

    public static double robust (Map<String, Integer> v1,
				 Map<String, Integer> v2,
				 double lamda) {
	Set<String> common = new TreeSet<String>(v1.keySet());
	common.retainAll(v2.keySet());

	double xy = 0., x = 0., y = 0.;
	for (String s : common) {
	    int c1 = v1.get(s), c2 = v2.get(s);
	    xy += c1*c2;
	    x += c1*c1;
	    y += c2*c2;
	}

	double z1 = 0., z2 = 0.;
	for (Integer c : v1.values()) {
	    z1 += c*c;
	}
	for (Integer c : v2.values()) {
	    z2 += c*c;
	}

	double a = xy / Math.sqrt(x*y);
	//double b = xy / Math.sqrt(z1*z2);
	double b = x*y / (z1*z2);

	return lamda*a + (1-lamda)*b;
    }

    public static double robust (Map<String, Integer> v1,
				 Map<String, Integer> v2) {
	return robust (v1, v2, 0.5);
    }

    public static double jaccard (Map<String, Integer> v1,
				  Map<String, Integer> v2) {
	Set<String> common = new TreeSet<String>(v1.keySet());
	common.retainAll(v2.keySet());

	double xy = 0., x = 0., y = 0.;
	for (String s : common) {
	    int c1 = v1.get(s), c2 = v2.get(s);
	    xy += c1*c2;
	}

	Set<String> notA = new TreeSet<String>(v1.keySet());
	notA.removeAll(common);
	for (String s : notA) {
	    int c = v1.get(s);
	    x += c*c;
	}
	Set<String> notB = new TreeSet<String>(v2.keySet());
	notB.removeAll(common);
	for (String s : notB) {
	    int c = v2.get(s);
	    y += c*c;
	}
	return xy/(x + y+xy);
    }

    public static double dice (Map<String, Integer> v1,
			       Map<String, Integer> v2) {
	Set<String> common = new TreeSet<String>(v1.keySet());
	common.retainAll(v2.keySet());
	double xy = 0., x = 0., y = 0.;
	for (String s : common) {
	    int c1 = v1.get(s), c2 = v2.get(s);
	    xy += c1*c2;
	}
	for (Integer c : v1.values()) {
	    x += c*c;
	}
	for (Integer c : v2.values()) {
	    y += c*c;
	}
	return 2.*xy/(x+y);
    }

    public static double tanimoto (Map<String, Integer> v1,
				   Map<String, Integer> v2) {
	Set<String> common = new TreeSet<String>(v1.keySet());
	common.retainAll(v2.keySet());
	double xy = 0., x = 0., y = 0.;
	for (String s : common) {
	    int c1 = v1.get(s), c2 = v2.get(s);
	    xy += Math.max(c1,c2);
	}
	for (Integer c : v1.values()) {
	    x += c;
	}
	for (Integer c : v2.values()) {
	    y += c;
	}
	return xy / (x + y - xy);
    }

    /*
     * similarity metric used in the LINGO paper
     */
    public static double lingo (Map<String, Integer> v1,
				Map<String, Integer> v2) {
	double sum = 0.;
	for (Map.Entry<String, Integer> e : v1.entrySet()) {
	    int c1 = e.getValue();
	    Integer x = v2.get(e.getKey());
	    int c2 = x != null ? x : 0;
	    sum += 1. - Math.abs(c1-c2)/(c1+c2);
	}

	int length = v1.size();
	for (Map.Entry<String, Integer> e : v2.entrySet()) {
	    int c2 = e.getValue();
	    Integer x = v1.get(e.getKey());
	    int c1 = 0;
	    if (x == null) {
		++length;
	    }
	    else {
		c1 = x;
	    }
	    sum += 1. - Math.abs(c1-c2)/(c1+c2);
	}

	return sum/length;
    }

    /*
     * Similarity metric used in the original Atom Pair paper
     */
    public static double sim (Map<String, Integer> v1,
			      Map<String, Integer> v2) {
	int sum = 0, a = 0;
	for (Map.Entry<String, Integer> e : v1.entrySet()) {
	    Integer ab = v2.get(e.getKey());
	    if (ab != null) {
		sum += Math.min(ab, e.getValue());
	    }
	    a += e.getValue();
	}
	int b = 0;
	for (Integer c : v2.values()) {
	    b += c;
	}

	return 2.*sum/(a + b);
    }
}
