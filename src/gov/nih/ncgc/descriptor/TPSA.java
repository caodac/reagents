// $Id: TPSA.java 2461 2009-03-01 23:12:12Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.io.InputStream;
import java.io.IOException;

public class TPSA extends GroupContrib {
    private static TPSA instance = null;
    
    private TPSA (InputStream is) throws IOException {
	super (is);
    }

    public synchronized static GroupContrib getInstance () {
	if (instance == null) {
	    try {
		instance = new TPSA
		    (TPSA.class.getResourceAsStream("resources/tpsa.rules"));
	    }
	    catch (IOException ex) {
		ex.printStackTrace();
	    }
	}
	return instance;
    }
}
