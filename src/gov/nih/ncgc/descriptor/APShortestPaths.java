// $Id$

package gov.nih.ncgc.descriptor;

import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.struc.MolBond;

import gov.nih.ncgc.util.MolPath;

public class APShortestPaths 
    extends MolPath.ShortestPaths 
    implements SparseDescriptor, MolPath.Annotator {
    
    public APShortestPaths (Molecule mol) {
	super (mol);
    }

    @Override
    public String toString (MolBond[] path) {
	return MolPath.toString(path, this);
    }

    // use the same atom typing used in AtomPair
    public String getLabel (MolAtom a) {
	int pi = AtomPair.numPiElectrons(a);
	int nb = a.getBondCount();
	return a.getSymbol() + pi + nb;
    }
    public String getLabel (MolBond b) { return ""; }
}
