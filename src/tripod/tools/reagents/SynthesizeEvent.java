
package tripod.tools.reagents;

import java.util.EventObject;
import chemaxon.struc.Molecule;

public class SynthesizeEvent extends EventObject {
    private Molecule mol; // synthesized molecule
    private int nth;
    private int size;

    public SynthesizeEvent (Object source, Molecule mol, int nth, int size) {
	super (source);
	this.mol = mol;
	this.nth = nth;
	this.size = size;
    }

    public Molecule getMol () { return mol; }
    public int size () { return size; }
    public int index () { return nth; }
}
