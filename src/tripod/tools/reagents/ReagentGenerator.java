
package tripod.tools.reagents;

import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingworker.SwingWorker;
import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import com.jgoodies.looks.plastic.theme.*;


public class ReagentGenerator extends JFrame implements WizardListener {
    private static final Logger logger = 
	Logger.getLogger(ReagentGenerator.class.getName());

    private Wizard wizard;
    public ReagentGenerator (String... argv) {
	ReagentGeneratorModel model = new ReagentGeneratorModel ();
	wizard = new Wizard (model);
	wizard.setDefaultExitMode(Wizard.EXIT_ON_FINISH);
	wizard.addWizardListener(this);
	wizard.setOverviewVisible(true);

	if (argv.length > 0) {
	    try {
		File file = new File (argv[0]);
		model.load(file);
		model.lastStep();
		setTitle ("NCGC Library Synthesizer: "+file.getName());
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Can't load project", ex);
	    }
	}
	else {
	    setTitle ("NCGC Library Synthesizer");
	}
	//setIconImage (ICON_BIOASSAY.getImage());
	setDefaultCloseOperation (DO_NOTHING_ON_CLOSE);
	getContentPane().add(wizard);
	pack ();
    }

    public void wizardCancelled (WizardEvent e) {
	exitApp ();
    }

    public void wizardClosed (WizardEvent e) {
	exitApp ();
    }

    void exitApp () {
	exitApp (false);
    }

    void exitApp (boolean force) {
	if (force) {
	    System.exit(0);
	}

	int ans = JOptionPane.showConfirmDialog
	    (this, "Are you sure you want to quit?", 
	     "Confirmation", JOptionPane.YES_NO_OPTION);
	if (ans == JOptionPane.YES_OPTION) {
	    System.exit(0);
	}
    }

    static void launch (final String[] argv) throws Exception {
	Plastic3DLookAndFeel.setPlasticTheme(new DesertBlue ());
	try {
	    UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
	} catch (Exception e) {}

	javax.swing.SwingUtilities.invokeLater
	    (new Runnable () {
		    public void run () {
			ReagentGenerator rg = new ReagentGenerator (argv);
			rg.setSize(600, 600);
			rg.setVisible(true);
		    }
		});
    }

    public static void main (final String argv[]) throws Exception {
	launch (argv);
    }

    public static class JnlpLaunch {
	public static void main (String[] argv) throws Exception {
	    StepCommon.JNLP = true;
	    launch (argv);
	}
    }
}
