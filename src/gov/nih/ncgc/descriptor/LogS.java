// $Id: LogS.java 2461 2009-03-01 23:12:12Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.io.InputStream;
import java.io.IOException;
import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.util.MolHandler;
import chemaxon.formats.MolImporter;

import static gov.nih.ncgc.descriptor.PattyTyper.Type;

public class LogS extends GroupContrib {
    private static LogS instance = null;
    static {
	try {
	    instance = new LogS
		(LogS.class.getResourceAsStream("resources/logS.rules"));
	}
	catch (IOException ex) {
	    ex.printStackTrace();
	}
    }
    
    private LogS (InputStream is) throws IOException {
	super (is);
    }

    public static GroupContrib getInstance () {
	return instance;
    }

    public double getValue (Molecule mol) {
	double C = 0.518; 	// correction factor
	Type[] types = PattyTyper.getInstance().getTypes(mol);
	for (int i = 0; i < types.length; ++i) {
	    MolAtom a = mol.getAtom(i);
	    // hydrophobic carbon
	    if (a.getAtno() == 6 && types[i] == Type.HYD) {
		C += -0.230;
	    }
	}

	double mw = new MolHandler (mol).calcMolWeightInDouble();
	C += mw*mw*0.00008;

	return super.getValue(mol) + C;
    }

    public static void main (String[] argv) throws Exception {
	if (argv.length < 1) {
	    System.out.println("usage: LogS FILES...");
	    System.exit(1);
	}
	
	GroupContrib gc = LogS.getInstance();
	for (int i = 0; i < argv.length; ++i) {
	    MolImporter mi = new MolImporter (argv[i]);
	    for (Molecule mol = new Molecule (); mi.read(mol); ) {
		System.out.println(mol.getName() + " " + gc.getValue(mol));
	    }
	    mi.close();
	}
    }
}
