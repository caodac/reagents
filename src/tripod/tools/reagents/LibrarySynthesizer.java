
package tripod.tools.reagents;

import java.util.*;
import java.util.logging.Logger;
import java.io.InputStream;
import java.io.IOException;

import chemaxon.struc.*;
import chemaxon.formats.*;
import chemaxon.util.MolHandler;

import gov.nih.ncgc.util.GrayCode;
import gov.nih.ncgc.util.ChemUtil;


public class LibrarySynthesizer  {
    private static final Logger logger = 
	Logger.getLogger(LibrarySynthesizer.class.getName());

    static class MolAttach {
	int anchor;

	MolAttach () {}
	// the number of possible attachments at this position
	int size () { return 0; }
	Object getAttachment (int position) { return null; }
	void setAnchor (int anchor) { this.anchor = anchor; }
	int getAnchor () { return anchor; }
    }

    static class MolAttachList extends MolAttach {
	int[] list;

	MolAttachList (int anchor, int[] list) {
	    setAnchor (anchor);
	    this.list = list;
	}

	int size () { return list.length; }
	Object getAttachment (int pos) { return list[pos]; }
    }

    static class MolAttachRGroup extends MolAttach {
	Molecule[] reagents;
	MolAttachRGroup (int anchor, Molecule[] reagents) {
	    setAnchor (anchor);
	    this.reagents = reagents;
	}
	int size () { return reagents.length; }
	Object getAttachment (int pos) { return reagents[pos]; }
    }

    class Generator implements Observer {
	MolAttach[] attachments;
	Molecule core;
	int size, total;

	Generator (Molecule core, MolAttach[] attachments) {
	    this.attachments = attachments;
	    int[] vector = new int[attachments.length];

	    total = 1; // total expected size of generated library
	    System.err.print(">> enum space {");
	    for (int i = 0; i < vector.length; ++i) {
		vector[i] = attachments[i].size();
		total *= vector[i];
		System.err.print(vector[i]);
		if (i+1 < vector.length) System.err.print(",");
	    }
	    System.err.println("}");

	    this.core = core;
	    GrayCode gcode = new GrayCode (vector);
	    gcode.setMaxSize(maxsize);
	    gcode.addObserver(this);
	    gcode.generate();
	}

	public void update (Observable obs, Object arg) {
	    int[] vector = (int[])arg;
	    StringBuilder sb = new StringBuilder ("[");
	    for (int i = 0; i < vector.length; ++i) {
		sb.append(vector[i]);
		if (i+1 < vector.length) sb.append(" ");
	    }
	    sb.append("]");

	    try {
		Molecule mol = generate (vector);
		if (mol != null) {
		    SynthesizeEvent e = new SynthesizeEvent 
			(LibrarySynthesizer.this, mol, size, total);
		    for (SynthesizeListener l : listeners) {
			l.synthesized(e);
		    }
		}
		/*
		System.err.println("++ enum vector "+sb
				   +" => "+mol.toFormat("smiles"));
		*/
	    }
	    catch (Exception ex) {
		logger.warning("Can't generate molecule for vector "
			       +sb+": "+ex.getMessage());
		ex.printStackTrace();
	    }
	}

	Molecule generate (int[] vector) {
	    Molecule mol = core.cloneMolecule();

	    List<MolAtom> atoms = new ArrayList<MolAtom>();
	    for (MolAtom a : mol.getAtomArray()) {
		atoms.add(a);
	    }

	    List<Molecule> reagents = new ArrayList<Molecule>();

	    int[] rsizes = mol.getSmallestRingSizeForIdx();
	    List<MolAtom> rgatoms = new ArrayList<MolAtom>();
	    for (int i = 0; i < vector.length; ++i) {
		MolAttach ma = attachments[i];
		Object att = ma.getAttachment(vector[i]);
		MolAtom atom = mol.getAtom(ma.getAnchor());
		if (atom.getAtno() == MolAtom.LIST) {
		    atom.setAtno((Integer)att);
		    atom.setSetSeq(i+1);
		}
		else if (atom.getAtno() == MolAtom.RGROUP) { // RGroup
		    /*
		    MolAtom xa = atom.getBond(0).getOtherAtom(atom);
		    mol.fuse(((Molecule)att).cloneMolecule());
		    // find the attachment point
		    for (MolAtom a : mol.getAtomArray()) {
			if (a.getAttach() != 0) {
			    a.setAttach(0);
			    MolBond b = new MolBond 
				(xa, a, atom.getBond(0).getFlags());
			    mol.add(b);
			}
		    }
		    remove.add(atom);
		    */
		    // The reagent (att) contains atom mappings for atoms
		    // that match the starting material. The atom that
		    // matches with the R-group is designated as an attachment
		    // point. Before we can fuse the reagent, we need to
		    // remove all atom with mapping info (except for the
		    // one with the attachment).
		    Integer reagentID = reagentIDs.get((Molecule)att);
		    Molecule reagent = ((Molecule)att).cloneMolecule();

		    MolAtom attach = peel (reagent);
		    if (attach == null) {
			logger.severe("Reagent has no attachment point!");
			return null;
		    }
		    reagent.valenceCheck();

		    if (saltStripping) {
			Molecule[] frags = reagent.convertToFrags();
			// keeping only fragment that contains the attachment
			for (Molecule f : frags) {
			    if (f.indexOf(attach) >= 0) {
				reagent = f;
			    }
			}
		    }

		    for (MolAtom a : reagent.getAtomArray()) {
			if (a != attach) {
			    a.setAttach(0);
			    a.setAtomMap(0);
			}
		    }

		    { Molecule r = reagent.cloneMolecule();
			r.setPropertyObject("REAGENT_ID", reagentID);
			MolAtom xa = null;
			for (MolAtom a : r.getAtomArray()) {
			    if (a.getAttach() != 0 && xa == null) {
				xa = a;
			    }
			    a.setAttach(0);
			    a.setAtomMap(0);
			}

			if (xa != null) {
			    MolAtom a = (MolAtom)atom.clone();
			    r.add(a);
			    MolBond bond = new MolBond (xa, a, 1);
			    r.add(bond);
			    reagents.add(r);
			}
		    }

		    attach.setAttach(0);
		    attach.setAtomMap(0);
		    
		    MolBond xb = atom.getBond(0);
		    MolAtom xa = xb.getOtherAtom(atom);
		    mol.fuse(reagent);
		    MolBond bond = new MolBond (xa, attach, xb.getFlags());
		    mol.add(bond);
		    int[] rs = reagent.getSmallestRingSizeForIdx();
		    if (rs[reagent.indexOf(attach)] <= 0) {
			attach.setXY(atom.getX(), atom.getY());
			atoms.add(attach);
		    }
		    for (MolAtom a : reagent.getAtomArray()) {
			a.setSetSeq(i+1);
		    }

		    rgatoms.add(atom); // remove this R-group atom
		}
		else {
		    logger.severe("Anchor "+(ma.getAnchor()+1)
				  +" is neither LIST nor RGROUP!");
		}
	    }
	    ++size;

	    for (MolAtom a : rgatoms) {
		mol.removeNode(a);
	    }

	    mol.valenceCheck();
	    if (mol.isQuery()) {
		throw new IllegalStateException 
		    ("Not all query atoms have been enumerated!");
	    }

	    if (!reagents.isEmpty()) {
		StringBuilder sb = new StringBuilder ();
		for (Molecule r : reagents) {
		    if (sb.length() > 0) {
			sb.append("\n");
		    }
		    sb.append(r.toFormat("cxsmarts:q")
			      +"\t"+r.getPropertyObject("REAGENT_ID"));
		}
		mol.setProperty("REAGENTS", sb.toString());
	    }

	    // now clean this molecule
	    if (false) { 
		BitSet set = new BitSet (atoms.size());
		for (MolAtom a : atoms) {
		    int i = mol.indexOf(a);
		    if (i >= 0) {
			set.set(i);
		    }
		}
		
		if (set.isEmpty()) {
		    logger.severe("Bogus molecule generated: "
				  +mol.toFormat("smiles"));
		}
		else {
		    int[] fixed = new int[set.cardinality()];
		    for (int i = set.nextSetBit(0), j = 0; 
			 i>=0; i = set.nextSetBit(i+1)) {
			fixed[j++] = i;
		    }
		    mol.partialClean(2, fixed, "O2");
		}
	    }
	    else {
		mol.clean(2, null);
	    }
	    ChemUtil.resetEZ(mol);

	    return mol;
	}

	int size () { return size; }
    }

    /**
     * Find the attachment point for which its connected component
     * contains at least one terminal atom.
     */
    static MolAtom peel (Molecule mol) {
	List<MolAtom> attachments = new ArrayList<MolAtom>();
	List<MolAtom> labeled = new ArrayList<MolAtom>();

	for (MolAtom a : mol.getAtomArray()) {
	    // attachment position can't be on a terminal atom
	    if (a.getAttach() != 0) {
		if (mol.getAtomCount() == 2 || !a.isTerminalAtom()) {
		    attachments.add(a);
		}
	    }
	    else if (a.getAtomMap() > 0) {
		labeled.add(a);
	    }
	}

	if (attachments.isEmpty()) {
	    return null;
	}
	else if (attachments.size() == 1) {
	    MolAtom attach = attachments.get(0);
	    for (MolAtom a : labeled) {
		if (a != attach) {
		    mol.removeNode(a);
		}
	    }
	    return attach;
	}
	
	// multiple attachments, so find the most likely one
	for (MolAtom attach : attachments) {
	    // the first one that 
	    List<MolAtom> visited = new ArrayList<MolAtom>();
	    dfs (attach, visited);
	    int terminals = 0;
	    for (MolAtom a : visited) {
		if (a.isTerminalAtom()) {
		    ++terminals;
		}
	    }
	    if (terminals > 0) { // we pick this attachment
		for (MolAtom a : visited) {
		    if (a != attach) {
			mol.removeNode(a);
		    }
		}

		return attach;
	    }
	}

	logger.warning("Attachment has no terminal atoms!");
	return null;
    }

    static void dfs (MolAtom node, List<MolAtom> visited) {
	visited.add(node);
	for (int i = 0; i < node.getBondCount(); ++i) {
	    MolAtom xn = node.getBond(i).getOtherAtom(node);
	    if (visited.indexOf(xn) < 0 
		&& xn.getAtomMap() > 0 
		&& xn.getAttach() == 0) {
		dfs (xn, visited);
	    }
	}
    }

    private List<SynthesizeListener> listeners = 
	new ArrayList<SynthesizeListener>();

    private Molecule core;

    // reagents organized by R-group
    private Map<Integer, Molecule[]> reagents = 
	new TreeMap<Integer, Molecule[]>();
    // assign unique id to each reagent for identification purposes
    private Map<Molecule, Integer> reagentIDs = 
	new HashMap<Molecule, Integer>();

    private boolean saltStripping = true;
    private int maxsize = 0; // maximum allowed

    public LibrarySynthesizer () {
    }

    public LibrarySynthesizer (Molecule core) {
	setCore (core);
    }

    public void setSaltStripping (boolean saltStripping) {
	this.saltStripping = saltStripping;
    }
    public boolean getSaltStripping () { return saltStripping; }

    public void setMaxSize (int maxsize) {
	this.maxsize = maxsize;
    }
    public int getMaxSize () { return maxsize; }

    public void setReagents (int rgroup, Molecule... reagents) {
	this.reagents.remove(rgroup);
	addReagents (rgroup, reagents);
    }

    public void addReagents (int rgroup, Molecule... reagents) {
	List<Molecule> rg = new ArrayList<Molecule>();

	Molecule[] mols = this.reagents.get(rgroup);
	if (mols != null) {
	    for (Molecule m : mols) {
		rg.add(m);
	    }
	}

	for (Molecule m : reagents) {
	    // if the reagent molecule doesn't have at least one attachments
	    //  then we generate all possible attachments
	    int natt = 0;

	    MolAtom[] atoms = m.getAtomArray();
	    for (MolAtom a : atoms) {
		if (a.getAttach() != 0) {
		    ++natt;
		}
	    }

	    if (natt == 0) {
		for (MolAtom a : atoms) {
		    if (a.getImplicitHcount() > 0) {
			a.setAttach(MolAtom.ATTACH1);
			rg.add(m.cloneMolecule());
			a.setAttach(0);
		    }
		}
	    }
	    else {
		rg.add(m);
	    }

	    reagentIDs.put(m, reagentIDs.size()+1);
	}

	this.reagents.put(rgroup, rg.toArray(new Molecule[0]));
    }

    public void setReagents (int rgroup, InputStream is) throws IOException {
	List<Molecule> reagents = new ArrayList<Molecule>();
	try {
	    MolImporter mi = new MolImporter (is);
	    for (Molecule m; (m = mi.read()) != null; ) {
		m.hydrogenize(false);
		m.expandSgroups();

		int att = 0;
		for (MolAtom a : m.getAtomArray()) {
		    if (a.getAttach() == MolAtom.ATTACH1 
			|| a.getAttach() == MolAtom.ATTACH2) {
			++att;
		    }
		}

		if (att > 0) {
		    reagents.add(m);
		}
		else {
		    logger.warning("Ignoring "+m.getName()
				   +"; reagent contains no attachment point!");
		}
	    }
	    mi.close();
	}
	catch (MolFormatException ex) {
	    logger.severe("Not all reagents are parsed for group "+rgroup);
	}

	if (!reagents.isEmpty()) {
	    this.reagents.put(rgroup, reagents.toArray(new Molecule[0]));
	}
    }

    public void setCore (Molecule core) {
	this.core = core;
    }

    public void addSynthesizeListener (SynthesizeListener l) {
	listeners.add(l);
    }
    public void removeSynthesizeListener (SynthesizeListener l) {
	listeners.remove(l);
    }

    public int synthesize () {
	MolAttach[] attachments = getAttachments ();
	logger.info(attachments.length
		    +" attachments; expected library size "
		    + getExpectedSize (attachments));
	Generator g = new Generator (core, attachments);

	return g.size();
    }

    protected MolAttach[] getAttachments () {
	if (core == null) {
	    throw new IllegalStateException ("No core specified!");
	}

	List<MolAttach> attachments = new ArrayList<MolAttach>();
	MolAtom[] atoms = core.getAtomArray();

	for (int i = 0; i < atoms.length; ++i) {
	    int atno = atoms[i].getAtno();
	    MolAttach att = null;
	    if (atno == MolAtom.LIST) {
		int[] l = atoms[i].getList();
		if (l == null || l.length == 0) {
		    throw new IllegalStateException ("Empty atom list");
		}
		logger.info("L attachment at "+(i+1));
		att = new MolAttachList (i, l);
	    }
	    else if (atno == MolAtom.RGROUP) {
		int rg = atoms[i].getRgroup();
		Molecule[] re = reagents.get(rg);
		if (re == null) {
		    throw new IllegalStateException 
			("No reagents defined for R group "+rg+"!");
		}
		logger.info("R attachment at "+(i+1));
		att = new MolAttachRGroup (i, re);
	    }

	    if (att != null) {
		attachments.add(att);
	    }
	}

	return attachments.toArray(new MolAttach[0]);
    }

    public int getExpectedSize () {
	return getExpectedSize (getAttachments ());
    }

    int getExpectedSize (MolAttach... att) {
	int size = 0; // expected size of the enumerated library
	for (MolAttach a : att) {
	    if (size == 0) size = a.size();
	    else size *= a.size();
	}
	return size;
    }

    public static void main (String[] argv) throws Exception {
	MolImporter mi = new MolImporter 
	    (LibrarySynthesizer.class
	     .getResourceAsStream("resources/core.xml"));
	Molecule core = mi.read();
	
	mi = new MolImporter 
	    (LibrarySynthesizer.class
	     .getResourceAsStream("resources/r1.xml"));
	Molecule reagent1 = mi.read();

	mi = new MolImporter 
	    (LibrarySynthesizer.class
	     .getResourceAsStream("resources/r3-1.xml"));
	Molecule reagent3 = mi.read();
	
	LibrarySynthesizer ls = new LibrarySynthesizer (core);
	ls.addSynthesizeListener(new SynthesizeListener () {
		public void synthesized (SynthesizeEvent e) {
		    Molecule m = e.getMol();
		    System.out.print(m.toFormat("mrv"));
		}
	    });
	ls.setReagents(1, reagent1);
	ls.setReagents(3, reagent3);
	ls.synthesize();
    }
}
