package tripod.tools.reagents;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import chemaxon.struc.*;

public class StepCalcProperties extends StepCommon {
    private static final Logger logger = Logger.getLogger
	(StepCalcProperties.class.getName());

    public static final String NAME = "Calculated Properties";
    public static final String DESC = 
	"Please check all properties to calculate for the generated libary.";


    public StepCalcProperties () {
	super (NAME, DESC);
    }

    @Override
    protected void initUI () {
    }

    @Override
    public void prepare () {
    }

    @Override
    public void applyState () throws InvalidStateException {
    }
}
