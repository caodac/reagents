package tripod.tools.reagents;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;
import java.text.NumberFormat;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

import javax.swing.plaf.UIResource;

import org.jdesktop.swingx.*;
import org.jdesktop.swingx.table.*;
import org.jdesktop.swingx.decorator.*;
import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.color.GradientTrackRenderer;
import org.jdesktop.swingx.color.GradientThumbRenderer;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.event.ListEventListener;
import ca.odell.glazedlists.event.ListEvent;
import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.FilterList;
import ca.odell.glazedlists.SortedList;
import ca.odell.glazedlists.PopularityList;
import ca.odell.glazedlists.matchers.Matcher;
import ca.odell.glazedlists.matchers.AbstractMatcherEditor;
import ca.odell.glazedlists.matchers.MatcherEditor;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import net.miginfocom.swing.MigLayout;
import com.jidesoft.swing.RangeSlider;
import com.jidesoft.swing.JideTabbedPane;

import chemaxon.struc.*;
import chemaxon.marvin.beans.MViewPane;
import chemaxon.marvin.beans.MSketchPane;
import chemaxon.util.MolHandler;

import gov.nih.ncgc.descriptor.*;
import gov.nih.ncgc.util.ChemUtil;
import gov.nih.ncgc.math.Histogram;

import tripod.ui.util.UIUtil;
import tripod.ui.base.Grid;
import tripod.ui.base.GridCellAnnotator;
import tripod.ui.base.MolCellEditor;
import tripod.ui.base.MolCellRenderer;
import tripod.ui.base.HistogramPlotPane;


public class StepGenerate extends StepCommon 
    implements GridCellAnnotator, ActionListener {

    private static final Logger logger = Logger.getLogger
	(StepGenerate.class.getName());

    public static final String NAME = "Generate Library";
    public static final String DESC = "Select all properties to calculate; click on the \"Synthesize\" button to begin library synthesis.";

    static abstract class MolProp {
	String name;
	String desc;
	boolean isdef; // is default?
	Number min, max;
	RangeSlider slider;
	HistogramPlotPane hpp;

	MolProp (String name, String desc, boolean isdef) {
	    this.name = name;
	    this.desc = desc;
	    this.isdef = isdef;
	}
	abstract Number calc (Molecule mol);
	boolean isDefault () { return isdef; }

	double getValue (int x) {
	    double a = min.doubleValue();
	    return a + x*(max.doubleValue()-a)/100.;
	}
	double getSliderLow () {
	    return getValue (slider.getLowValue());
	}
	double getSliderHigh () {
	    return getValue (slider.getHighValue());
	}
    }

    class TabRowSlider extends JPanel implements UIResource {
	JSlider slider;
	Object source;

	TabRowSlider () {
	    this (20, 200);
	}

	TabRowSlider (int min, int max) {
	    slider = new JSlider (min, max);
	    slider.setBorder(null);
	    add (slider);
	}

	void setModel (BoundedRangeModel model) {
	    slider.setModel(model);
	}

	void setValue (int value) { 
	    slider.setValue(value);
	}
	int getValue () { return slider.getValue(); }
    }

    class TabLabel extends JLabel implements UIResource {
	TabLabel () {
	}
    }

    class SynthesizeWorker extends SwingWorker<Throwable, Integer> 
	implements SynthesizeListener {

	LibrarySynthesizer synth;
	PropsTableModel model;
	int max;

	SynthesizeWorker (int max, PropsTableModel model, 
			  LibrarySynthesizer synth) {
	    this.synth = synth;
	    if (max <= 0) {
		max = synth.getExpectedSize();
	    }
	    this.max = max;
	    this.model = model;
	    synth.addSynthesizeListener(this);
	}

	@Override
	protected Throwable doInBackground () {
	    setBusy (true);
	    try {
		synth.synthesize();
	    }
	    catch (Exception ex) {
		return ex;
	    }
	    return null;
	}

	NumberFormat nf = NumberFormat.getInstance();

	@Override
	protected void process (Integer... indexes) {
	    for (Integer i : indexes) {
		statusField.setText(nf.format(i));
	    }
	}

	@Override
	protected void done () {
	    synth.removeSynthesizeListener(this);
	    setBusy (false);
	    try {
		Throwable t = get ();
		if (t != null) {
		    logger.log(Level.SEVERE, 
			       "Error encountered during library sysnthesis", 
			       t);
		}
		else {
		    updateFilterBounds ();
		    resultFrame.setTitle("Synthesized Results ("
					 +NumberFormat.getInstance().format
					 (resultTable.getRowCount())+")");
		}
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Error encountered during library sysnthesis", 
			   ex);
	    }
	}

	public void synthesized (SynthesizeEvent e) {
	    Molecule mol = e.getMol();
	    model.add(mol);

	    setProgress (100*e.index()/max);
	    publish (e.index());
	}
    }

    class PropsTableModel extends AbstractTableModel implements Comparator {
	MolProp[] props;
	java.util.List<Molecule> mols = new ArrayList<Molecule>();
	java.util.List<Object[]> rows = new ArrayList<Object[]>();

	FilterList reagents = new FilterList 
	    //(new SortedList (new BasicEventList(), this));
	    (new BasicEventList ());
	ReagentFilter filter;

	// reagent pool
	Map<String, Molecule> pool = new HashMap<String, Molecule>();
	// reagent mapping from the parent molecule
	Map<Molecule, Molecule[]> reamap = new HashMap<Molecule, Molecule[]>();

	PropsTableModel () {
	}

	PropsTableModel (MolProp[] props) {
	    this.props = props;
	}

	void setProps (MolProp[] props) {
	    this.props = props;
	    mols.clear();
	    rows.clear();
	    reagents.clear();
	    pool.clear();
	    reamap.clear();
	    fireTableStructureChanged ();
	}
	MolProp[] getProps () { return props; }
	
	int getPropColumn (String name) {
	    for (int i = 0; i < props.length; ++i) {
		if (props[i].name.equals(name)) {
		    return i+2;
		}
	    }
	    return -1;
	}

	int getColumn (MolProp prop) {
	    for (int i = 0; i < props.length; ++i) {
		if (prop == props[i]) {
		    return i+2;
		}
	    }
	    return -1;
	}

	Molecule getMol (int r) { return mols.get(r); }
	EventList getReagentSource () { return reagents; }
	void setReagentFilter (ReagentFilter filter) {
	    this.filter = filter;
	    reagents.setMatcher(filter);
	}
	ReagentFilter getReagentFilter () { return filter; }

	public int compare (Object o1, Object o2) {
	    if (filter == null) return -1;
	    return filter.compare(o1, o2);
	}

	Molecule[] getReagents (int r) {
	    return reamap.get(getMol (r));
	}

	public int getColumnCount () {
	    return 2 + (props != null ? props.length : 0);
	}
	public int getRowCount () { return rows.size(); }
	public String getColumnName (int col) {
	    if (col == 0) return "#";
	    if (col == 1) return "Compound";
	    return props[col-2].name;
	}
	public Object getValueAt (int row, int col) {
	    if (col == 0) return row+1;
	    if (col == 1) return mols.get(row);
	    return rows.get(row)[col-2];
	}
	public void add (Molecule... mols) {
	    for (Molecule m : mols) {
		Molecule mol = m.cloneMolecule(); 

		this.mols.add(mol);
		mol.setName(String.valueOf(this.mols.size()));

		Object[] r = new Object[props.length];
		for (int i = 0; i < props.length; ++i) {
		    try {
			r[i] = props[i].calc(m);
			if (r[i] != null) {
			    mol.setProperty(props[i].name, r[i].toString());
			}
		    }
		    catch (Exception ex) {
		    }
		}
		rows.add(r);

		Molecule[] reagent = parseReagents (mol);
		for (Molecule rg : reagent) {
		    if (reagents.indexOf(rg) < 0) {
			// sigh????
			reagents.add(rg);
		    }
		}
		reamap.put(mol, reagent);
	    }
	    fireTableDataChanged ();
	}

	Molecule[] parseReagents (Molecule mol) {
	    String str = mol.getProperty("REAGENTS");
	    if (str == null) {
		return new Molecule[0];
	    }
	    MolHandler mh = new MolHandler ();

	    java.util.List<Molecule> rl = new ArrayList<Molecule>();
	    for (String s : str.split("\n")) {
		String[] toks = s.split("\t");
		if (toks.length == 2) {
		    Molecule r = pool.get(toks[1]);
		    if (r == null) {
			try {
			    mh.setMolecule(toks[0]);
			    r = mh.getMolecule();
			    r.setName(toks[1]);
			    pool.put(toks[1], r);
			}
			catch (Exception ex) {
			    logger.log(Level.SEVERE, "Bad reagent: "+s, ex);
			}
		    }
		    rl.add(r);
		}
	    }

	    return rl.toArray(new Molecule[0]);
	}

	public Class getColumnClass (int col) {
	    if (col == 0) return Integer.class;
	    if (col == 1) return Molecule.class;
	    return Number.class;
	}

	public boolean isCellEditable (int row, int col) {
	    return col == 1;
	}
    }

    class ReagentGrid implements GridCellAnnotator {
	Grid grid;
	PropsTableModel model;

	ReagentGrid (PropsTableModel model) {
	    this.model = model;
	    grid = new Grid (model.getReagentSource());
	    grid.setCellRenderer(new MolCellRenderer ());
	    grid.setCellEditor(new MolCellEditor ());
	    grid.setCellAnnotator(this);
	}

	 
	Grid getGrid () { return grid; }
	public String getGridCellAnnotation (Grid g, Object value, int cell) {
	    //return ((Molecule)value).getName();
	    return null;
	}

	public String getGridCellBadgeAnnotation 
	    (Grid g, Object value, int cell) {
	    ReagentFilter filter = model.getReagentFilter();
	    if (filter == null) return null;
	    Integer c = filter.getCount(value);
	    return c != null ? c.toString() : null;
	}
    }

    class ReagentFilter implements Matcher, Comparator {
	// filtered reagents
	Map<Molecule, Integer> reagents = new HashMap<Molecule, Integer>();

	ReagentFilter () {
	}

	void add (Molecule... reagents) {
	    for (Molecule r : reagents) {
		Integer c = this.reagents.get(r);
		this.reagents.put(r, c==null?1:(1+c));
	    }
	}

	public boolean matches (Object o) {
	    return reagents.containsKey((Molecule)o);
	}

	public int compare (Object obj1, Object obj2) {
	    Integer c1 = reagents.get((Molecule)obj1);
	    Integer c2 = reagents.get((Molecule)obj2);
	    return c2 - c1;
	}

	public Integer getCount (Object obj) {
	    return reagents.get((Molecule)obj);
	}
    }

    class PropsTable extends JXTable implements PipelineListener {
	java.util.List<PropsFilter> filters;

	PropsTable () {
	    super (new PropsTableModel ());
	    setDefaultRenderer (Molecule.class, new MolCellRenderer ());
	    setDefaultEditor (Molecule.class, new MolCellEditor ());
	    setRowHeight (110);
	    setColumnControlVisible (true);
	    setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
	}

	PropsTableModel getPropsTableModel () {
	    return (PropsTableModel)getModel ();
	}

	@Override
	public void createDefaultColumnsFromModel () {
	    // keep the column location and size the same
	    TableColumnModel model = getColumnModel ();
	    Map<Integer, TableColumn> columns = 
		new HashMap<Integer, TableColumn>();
	    for (int c = 0; c < model.getColumnCount(); ++c) {
		TableColumn tc = model.getColumn(c);
		columns.put(tc.getModelIndex(), tc);
	    }
	    
	    TableModel tm = getModel ();
	    for (int c = 0; c < tm.getColumnCount(); ++c) {
		TableColumn tc = (TableColumn)columns.get(c);
		if (tc == null) {
		    tc = new TableColumnExt (c, 120);
		    tc.setHeaderValue(tm.getColumnName(c));
		    model.addColumn(tc);
		}
		else {
		    // make sure the column header is update
		    tc.setHeaderValue(tm.getColumnName(c));
		}
	    }
	    
	    for (Map.Entry<Integer, TableColumn> me : 
		     columns.entrySet()) {
		if (me.getKey() < tm.getColumnCount()) {
		}
		else {
		    // remove
		    model.removeColumn(me.getValue());
		}
	    }
	    JTableHeader header = getTableHeader ();
	    if (header != null) {
		header.repaint();
	    }
	}

	void setFilters (java.util.List<PropsFilter> filters) {
	    FilterPipeline pipeline = new FilterPipeline 
		(filters.toArray(new Filter[0]));
	    pipeline.addPipelineListener(this);
	    this.filters = filters;
	    super.setFilters(pipeline);
	}

	public void contentsChanged (PipelineEvent e) {
	    for (PropsFilter f : filters) {
		f.clear();
	    }

	    PropsTableModel model = getPropsTableModel ();

	    ReagentFilter rf = new ReagentFilter ();
	    for (int i = 0; i < getRowCount (); ++i) {
		int j = convertRowIndexToModel (i);
		for (PropsFilter f : filters) {
		    int c = model.getColumn(f.prop);
		    if (c < 0) {
			throw new IllegalStateException 
			    ("Property "+f.prop.name+" not in table model!");
		    }
		    else {
			Number x = (Number)model.getValueAt(j, c);
			f.increment(x);
		    }
		}

		rf.add(model.getReagents(j));
	    }

	    // update reagent filter
	    model.setReagentFilter(rf);

	    for (PropsFilter f : filters) {
		f.update();
	    }

	    /*
	    logger.info("Filter pipeline changed: "
			+((FilterPipeline)e.getSource()).getOutputSize()
			+" "+getRowCount());
	    */
	}

    }


    class ExportResultsWorker extends SwingWorker<Integer, Void> {
	JDialog diag;
	Exception ex;
	OutputStream os;

	ExportResultsWorker (JDialog diag, OutputStream os) {
	    this.diag = diag;
	    this.os = os;
	}

	@Override
	protected Integer doInBackground () {
	    int count = 0;
	    try {
		PrintStream ps = new PrintStream (os);
		PropsTableModel model = 
		    (PropsTableModel)resultTable.getModel();

		for (int i = 0; i < resultTable.getRowCount(); ++i) {
		    int r = resultTable.convertRowIndexToModel(i);
		    Molecule mol = model.getMol(r);
		    ps.print(mol.toFormat("sdf"));
		    ++count;
		}
		ps.close();
	    }
	    catch (Exception ex) {
		this.ex = ex;
	    }
	    return count;
	}

	@Override
	protected void done () {
	    diag.setVisible(false);
	    diag.dispose();
	    try {
		Integer cnt = get ();
		if (this.ex != null) {
		    JXErrorPane.showDialog(this.ex);
		}
		else {
		    JOptionPane.showMessageDialog
			(StepGenerate.this, cnt+" structures exported!", 
			 "Message", JOptionPane.INFORMATION_MESSAGE);
		}
	    }
	    catch (Exception ex) {
		JXErrorPane.showDialog(ex);
	    }
	}
    }

    class PropsFilter extends Filter implements ChangeListener {
	MolProp prop;
	Histogram hist;
	ArrayList<Integer> toPrevious;

	PropsFilter (int column, MolProp prop) {
	    super (column);
	    this.prop = prop;
	    prop.slider.addChangeListener(this);
	    hist = new Histogram 
		(20, prop.min.doubleValue(), prop.max.doubleValue());
	}

	protected void filter () {
	    int size = getInputSize ();
	    int column = getColumnIndex ();
	    //hist.clear();
	    for (int i = 0, j = 0; i < size; ++i) {
		Number x = (Number) getInputValue (i, column);
		if (x != null && withinRange (x)) {
		    hist.increment(x.doubleValue());
                    toPrevious.add(i);
                    // generate inverse map entry while we are here
                    fromPrevious[i] = j++;
		}
	    }
	    //logger.info("Filter "+prop.name+" applied");
	    //prop.hpp.setData(prop.name, hist);
	}

	public Histogram getHistogram () { return hist; }
	public boolean increment (Number x) {
	    if (x != null && withinRange (x)) {
		hist.increment(x.doubleValue());
		return true;
	    }
	    return false;
	}
	public void clear () { hist.clear(); }
	public void update () {
	    prop.hpp.setData(prop.name, hist);
	}

	public void stateChanged (ChangeEvent e) {
	    RangeSlider slider = (RangeSlider)e.getSource();
	    if (!slider.getValueIsAdjusting()) {
		//fireFilterChanged ();
		/*
		logger.info("Refreshing filter "+prop.name+": low="
			    +slider.getLowValue()+"/"
			    +prop.getValue(slider.getLowValue()) 
			    +" high="
			    +slider.getHighValue()+"/"
			    +prop.getValue(slider.getHighValue()));
		*/
	    }

	    refresh ();
	    //getPipeline().flush();
	}

	boolean withinRange (Number x) {
	    double low = prop.getSliderLow();
	    double high = prop.getSliderHigh();
	    return x.doubleValue() >= low && x.doubleValue() <= high;
	}

	public int getSize() {
	    return toPrevious.size();
	}
	
	protected int mapTowardModel(int row) {
	    return toPrevious.get(row);
	}
	
	protected void init() {
	    toPrevious = new ArrayList<Integer>();
	}
	
	protected void reset () {
	    int inputSize = getInputSize();
	    // fromPrevious is inherited protected
	    fromPrevious = new int[inputSize];  
	    for (int i = 0; i < inputSize; ++i) {
		fromPrevious[i] = -1;
	    }
	    toPrevious.clear();
	}
    }

    JCheckBox[] props;
    Grid grid;
    JTextField maxField;
    JCheckBox stripSaltCb;
    JButton synBtn, saveBtn;
    JTextField statusField;
    JProgressBar progress;
    JPanel filterPane;

    // reagent annotation 
    Map<String, Integer> anno = new HashMap<String, Integer>();
    LibrarySynthesizer synthesizer;

    JFrame resultFrame;
    PropsTable resultTable;
    ReagentGrid reagentGrid;

    public StepGenerate () {
	super (NAME, DESC);
    }

    @Override
    protected void initUI () {
	JPanel panel = new JPanel (new MigLayout ("", "[grow,fill]", ""));

	addSeparator (panel, "Reagents");
	panel.add(grid = createReagentPane (), "spanx,grow,wrap 10");

	addSeparator (panel, "Settings");
	JTabbedPane tab = new JideTabbedPane ();
	tab.addTab("Properties", createPropPane ());
	tab.addTab("Options", createOptionPane ());
	panel.add(tab, "spanx,grow,wrap 10");

	addSeparator (panel, "Status");
	panel.add(createControlPane (), "spanx,grow");
	
	setLayout (new BorderLayout ());
	add (panel);

	createResultWindow ();
    }

    void createResultWindow () {
	resultFrame = new JFrame ();
	//resultFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	
	JPanel pane = new JPanel (new BorderLayout ());
	pane.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));

	JSplitPane split = new JSplitPane ();
	split.setDividerSize(5);
	split.setLeftComponent(createFilterPane ());
	split.setRightComponent(createResultPane ());

	pane.add(split);
	resultFrame.getContentPane().add(pane);
	resultFrame.setPreferredSize(new Dimension (800, 500));
	resultFrame.pack();
	resultFrame.setTitle("Synthesized Results");
    }

    Component createResultPane () {
	resultTable = new PropsTable ();

	JideTabbedPane tab = new JideTabbedPane ();
	tab.addTab("Molecules", new JScrollPane (resultTable));
	final BoundedRangeModel tabRange = new DefaultBoundedRangeModel
	    (resultTable.getRowHeight(), 0, 50, 200);
	tabRange.addChangeListener(new ChangeListener () {
		public void stateChanged (ChangeEvent e) {
		    resultTable.setRowHeight(tabRange.getValue());
		}
	    });

	final TabRowSlider trs = new TabRowSlider ();
	trs.setModel(tabRange);

	reagentGrid = new ReagentGrid (resultTable.getPropsTableModel());
	tab.addTab("Reagents", new JScrollPane (reagentGrid.getGrid()));
	final BoundedRangeModel gridRange = new DefaultBoundedRangeModel
	    (reagentGrid.getGrid().getCellSize(), 0, 50, 200);
	gridRange.addChangeListener(new ChangeListener () {
		public void stateChanged (ChangeEvent e) {
		    reagentGrid.getGrid().setCellSize(gridRange.getValue());
		}
	    });
	tab.addChangeListener(new ChangeListener () {
		public void stateChanged (ChangeEvent e) {
		    JTabbedPane tab = (JTabbedPane)e.getSource();
		    int index = tab.getSelectedIndex();
		    if (index == 0) {
			trs.setModel(tabRange);
		    }
		    else if (index == 1) {
			trs.setModel(gridRange);
		    }
		}
	    });
	// default trailer
	tab.setTabTrailingComponent(trs);
	return tab;
    }

    Component createFilterPane () {
	JPanel bpane = new JPanel (new GridLayout (1,2,2,0));
	JButton btn = new JButton ("Reset");
	btn.setToolTipText("Reset filters");
	btn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    for (Component c : filterPane.getComponents()) {
			if (c instanceof RangeSlider) {
			    ((RangeSlider)c).setLowValue(0);
			    ((RangeSlider)c).setHighValue(100);
			}
		    }
		}
	    });
	bpane.add(btn);

	btn = new JButton ("Export");
	btn.setToolTipText("Export synthesized results as SD file");
	btn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    exportResults ();
		}
	    });
	bpane.add(btn);

	JPanel bbpane = new JPanel ();
	bbpane.add(bpane);

	JPanel pane = new JPanel (new BorderLayout (0, 5));
	pane.add(new JScrollPane (filterPane = new JPanel ()));
	filterPane.setBackground(Color.white);
	filterPane.setBorder
	    (BorderFactory.createCompoundBorder
	     (BorderFactory.createEmptyBorder(2,2,2,2),
	      BorderFactory.createTitledBorder("Property filters")));
	
	pane.add(bbpane, BorderLayout.SOUTH);

	return pane ;
    }

    void exportResults () {
	if (0 == resultTable.getRowCount()) {
	    JOptionPane.showMessageDialog
		(this, "No results to export!", 
		 "Message", JOptionPane.INFORMATION_MESSAGE);
	    return;
	}

	Frame root = (Frame)SwingUtilities
	    .getAncestorOfClass(Frame.class, this);
	JDialog diag = new JDialog (root, true);
	diag.setUndecorated(true);
	diag.setPreferredSize(new Dimension (250, 40));
	UIUtil.centerComponent(root, diag);

	final JProgressBar progress = new JProgressBar ();
	progress.setStringPainted(true);
	JPanel pane = new JPanel (new BorderLayout (0, 5));
	pane.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
	pane.add(new JLabel ("Exporting results..."), BorderLayout.NORTH);
	pane.add(progress);
	diag.getContentPane().add(pane);
	diag.pack();

	try {
	    OutputStream os = getOutputFileStream
		("Export synthesized results", "sd", "sdf", "mol");
	    if (os != null) {
		new ExportResultsWorker (diag, os).execute();
		diag.setVisible(true);
	    }
	}
	catch (IOException ex) {
	    JXErrorPane.showDialog(ex);
	}
    }

    void updateFilters (MolProp... props) {
	filterPane.removeAll();
	filterPane.setLayout(new MigLayout ("", "[right][grow,fill][]", ""));
	for (MolProp p : props) {
	    if (p.slider == null) {
		p.slider = new RangeSlider ();
		p.slider.setBackground(Color.white);
		p.slider.setPreferredSize(new Dimension (50, 20));
		p.slider.setPaintTrack(true);
		p.slider.setPaintTicks(true);
		p.slider.setLowValue(0);
		p.slider.setHighValue(100);
		//slider.setPaintLabels(true);
		p.slider.setBorder(BorderFactory.createEmptyBorder(3,3,3,3));
		final JLabel min = new JLabel ();
		p.slider.putClientProperty("MinLabel", min);
		final JLabel max = new JLabel ();
		p.slider.putClientProperty("MaxLabel", max);
		p.slider.putClientProperty("MolProp", p);
		p.slider.addChangeListener(new ChangeListener () {
			public void stateChanged (ChangeEvent e) {
			    MolProp p = (MolProp)((JComponent)e.getSource())
				.getClientProperty("MolProp");
			    min.setText(String.format
					("%1$.1f", p.getSliderLow()));
			    max.setText(String.format
					("%1$.1f", p.getSliderHigh()));
			    resultFrame.setTitle
				("Synthesized Results ("
				 +NumberFormat.getInstance().format
				 (resultTable.getRowCount())+")");
			}
		    });
		p.hpp = new HistogramPlotPane ();
		p.hpp.getPlot().getDomainAxis().setVisible(false);
		p.hpp.setPreferredSize(new Dimension (100, 50));
	    }

	    addSeparator (filterPane, p.name);
	    filterPane.add(p.hpp, "skip,wrap 10");
	    filterPane.add((Component)p.slider.getClientProperty("MinLabel"));
	    filterPane.add(p.slider);
	    filterPane.add((Component)p.slider.getClientProperty("MaxLabel"),
			   "wrap 10");
	}
    }

    void updateFilterBounds () {
	updateFilterBounds (getProperties ());
    }

    void updateFilterBounds (MolProp... props) {
	java.util.List<PropsFilter> filters = new ArrayList<PropsFilter>();
	PropsTableModel model = (PropsTableModel)resultTable.getModel();
	
	for (MolProp p : props) {
	    RangeSlider slider = p.slider;
	    for (ChangeListener l : slider.getChangeListeners()) {
		if (l instanceof Filter) {
		    slider.removeChangeListener(l);
		}
	    }
	    JLabel min = (JLabel)slider.getClientProperty("MinLabel");
	    JLabel max = (JLabel)slider.getClientProperty("MaxLabel");
	    if (p.min instanceof Integer) {
		min.setText(p.min.toString());
		max.setText(p.max.toString());
	    }
	    else {
		min.setText(String.format
			    ("%1$.1f", p.min.doubleValue()));
		max.setText(String.format
			    ("%1$.1f", p.max.doubleValue()));
	    }
	    PropsFilter filter = new PropsFilter 
		(model.getPropColumn(p.name), p);
	    filters.add(filter);
	}
	resultTable.setFilters(filters);

	filterPane.revalidate();
	filterPane.repaint();
    }

    Component createPropPane () {
	MolProp[] PROPS = new MolProp[] {
	    new MolProp ("H-bond donor", "H-bond donor", true) {
		Number calc (Molecule mol) {
		    int don = HBond.donors(mol);
		    if (min == null || don < min.intValue()) min = don;
		    if (max == null || don > max.intValue()) max = don;
		    return don;
		}
	    },
	    new MolProp ("H-bond acceptor", "H-bond acceptor", true) {
		Number calc (Molecule mol) {
		    int acc = HBond.acceptors(mol);
		    if (min == null || acc < min.intValue()) min = acc;
		    if (max == null || acc > max.intValue()) max = acc;
		    return acc;
		}
	    },
	    new MolProp ("Complexity", "Molecular complexity index", true) {
		Number calc (Molecule mol) {
		    int c = ChemUtil.complexity(mol);
		    if (min == null || c < min.intValue()) min = c;
		    if (max == null || c > max.intValue()) max = c;
		    return c;
		}
	    },
	    new MolProp ("Molecular weight", "Molecule weight", true) {
		Number calc (Molecule mol) {
		    double m = mol.getMass();
		    if (min == null || m < min.doubleValue()) min = m;
		    if (max == null || m > max.doubleValue()) max = m;
		    return m;
		}
	    },
	    new MolProp ("tPSA", "Topological Polar Surface Area", true) {
		Number calc (Molecule mol) {
		    double psa = TPSA.getInstance().getValue(mol);
		    if (min == null || psa < min.intValue()) min = psa;
		    if (max == null || psa > max.intValue()) max = psa;
		    return psa;
		}
	    },
	    new MolProp ("logP", "Estimated log P", true) {
		Number calc (Molecule mol) {
		    double x = LogP.getInstance().getValue(mol);
		    if (min == null || x < min.intValue()) min = x;
		    if (max == null || x > max.intValue()) max = x;
		    return x;
		}
	    },
	    new MolProp ("logS", "Estimated log S", true) {
		Number calc (Molecule mol) {
		    double x = LogS.getInstance().getValue(mol);
		    if (min == null || x < min.intValue()) min = x;
		    if (max == null || x > max.intValue()) max = x;
		    return x;
		}
	    },
	    new MolProp ("Molar refractivity", 
			 "Estimated molar refractivity index", true) {
		Number calc (Molecule mol) {
		    double x = MR.getInstance().getValue(mol);
		    if (min == null || x < min.intValue()) min = x;
		    if (max == null || x > max.intValue()) max = x;
		    return x;
		}
	    },
	    new MolProp ("SASA area", 
			 "Solvent accessible surface area", false) {
		Number calc (Molecule mol) {
		    double area = SASA.nsc(mol).getArea();
		    if (min == null || area < min.doubleValue()) min = area;
		    if (max == null || area > max.doubleValue()) max = area;
		    return area;
		}
	    },
	    new MolProp ("SASA volume", 
			 "Solvent accessible surface volume", false) {
		Number calc (Molecule mol) {
		    double v = SASA.nsc(mol).getVolume();
		    if (min == null || v < min.doubleValue()) min = v;
		    if (max == null || v > max.doubleValue()) max = v;
		    return v;
		}
	    }
	};

	JPanel ppane = new JPanel (new GridLayout (3, 4, 5, 5));
	props = new JCheckBox[PROPS.length];
	for (int i = 0; i < PROPS.length; ++i) {
	    MolProp p = PROPS[i];
	    JCheckBox cb = new JCheckBox (p.name);
	    cb.setToolTipText(p.desc);
	    cb.setSelected(p.isDefault());
	    cb.putClientProperty("MolProp", p);
	    ppane.add(cb);
	    props[i] = cb;
	}
	ppane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
	return ppane;
    }

    Component createOptionPane () {
	JPanel pane = new JPanel 
	    (new MigLayout ("wrap", "[right]unrel[left]",""));
	pane.add(new JLabel ("Library size"));
	pane.add(maxField = new JTextField (5));
	maxField.setToolTipText("Specify the maximum size of the library");
	pane.add(new JLabel ("Salt stripping"));
	pane.add(stripSaltCb = new JCheckBox ());
	stripSaltCb.setSelected(true);
	stripSaltCb.setToolTipText("Specify whether to remove "
				   +"salt/solvent from generated structures");
	JPanel ppane = new JPanel ();
	ppane.add(pane);
	return ppane;
    }

    Grid createReagentPane () {
	Grid grid = new Grid ();
	grid.setCellRenderer(new MolCellRenderer ());
	grid.setCellEditor(new MolCellEditor ());
	grid.setCellAnnotator(this);
	grid.setCellSize(120);
	return grid;
    }

    Component createControlPane () {
	JPanel bpane = new JPanel (new GridLayout (1, 2, 2, 0));
	JButton btn = new JButton ("Synthesize");
	btn.setToolTipText("Start library synthesis");
	bpane.add(btn);
	btn.addActionListener(this);
	synBtn = btn;
	btn = new JButton ("Save");
	btn.setToolTipText("Save project (not synthesized results)");
	saveBtn = btn;
	btn.addActionListener(this);
	bpane.add(btn);

	JPanel pane = new JPanel (new BorderLayout (5, 0));
	pane.add(bpane, BorderLayout.EAST);
	pane.add(progress = new JProgressBar (0, 100));
	statusField = new JTextField (5);
	statusField.setEditable(false);
	pane.add(statusField, BorderLayout.WEST);

	return pane;
    }

    @Override
    public void setBusy (boolean busy) {
	super.setBusy(busy);
	synBtn.setEnabled(!busy);
	setComplete (!busy);
    }

    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
	PropsTableModel model = (PropsTableModel)resultTable.getModel();

	if ("synthesize".equalsIgnoreCase(cmd)) {
	    MolProp[] props = getProperties ();
	    updateFilters (props);
	    model.setProps(props);

	    // clear any existing filters
	    resultTable.setFilters((FilterPipeline)null);
	    synthesizer.setSaltStripping(stripSaltCb.isSelected());
	    try {
		synthesizer.setMaxSize(Integer.parseInt(maxField.getText()));
	    }
	    catch (NumberFormatException ex) {
		JOptionPane.showMessageDialog
		    (StepGenerate.this, "Invalid max size: "
		     +maxField.getText(), "Error", JOptionPane.ERROR_MESSAGE);
		return;
	    }

	    SynthesizeWorker worker = 
		new SynthesizeWorker (synthesizer.getMaxSize(), 
				      model, synthesizer);
	    worker.addPropertyChangeListener(new PropertyChangeListener () {
		    public void propertyChange (PropertyChangeEvent e) {
			if ("progress".equals(e.getPropertyName())) {
			    progress.setValue((Integer)e.getNewValue());
			}
		    }
		});
	    worker.execute();
	}
	else if ("save".equalsIgnoreCase(cmd)) {
	    try {
		OutputStream os = getOutputFileStream ("Save project", "zip");
		if (os != null) {
		    this.model.save(os);
		    os.close();
		}
	    }
	    catch (Exception ex) {
		JXErrorPane.showDialog(ex);
	    }
	}
    }

    MolProp[] getProperties () {
	java.util.List<MolProp> props = new ArrayList<MolProp>();
	for (JCheckBox cb : this.props) {
	    if (cb.isSelected()) {
		MolProp p = (MolProp)cb.getClientProperty("MolProp");
		props.add(p);
	    }
	}

	return props.toArray(new MolProp[0]);
    }

    @Override
    public void prepare () {
	setComplete (false);
	grid.clear();
	anno.clear();

	for (JCheckBox cb : props) {
	    MolProp p = (MolProp)cb.getClientProperty("MolProp");
	    p.slider = null;
	    p.min = null;
	    p.max = null;
	}

	progress.setValue(0);

	Molecule mol = (Molecule)getModel().get("core");
	grid.addValue(mol);

	synthesizer = new LibrarySynthesizer ((Molecule)model.get("core"));
	Map<Integer, Molecule> materials = (Map)model.get("materials");
	Map<Integer, Molecule[]> reagents = (Map)model.get("reagents");

	if (materials != null && reagents != null) {
	    for (Map.Entry<Integer, Molecule> me : materials.entrySet()) {
		Molecule r = me.getValue();
		grid.addValue(r);
		int size = reagents.get(me.getKey()).length;
		anno.put(r.getName(), size);
	    }

	    for (Map.Entry<Integer, Molecule[]> me : reagents.entrySet()) {
		synthesizer.setReagents(me.getKey(), me.getValue());
	    }
	}

	resultFrame.setVisible(true);
	maxField.setText(String.valueOf(synthesizer.getExpectedSize()));
    }

    public String getGridCellAnnotation (Grid g, Object value, int cell) {
	return ((Molecule)value).getName();
    }

    public String getGridCellBadgeAnnotation (Grid g, Object value, int cell) {
	Integer c = anno.get(((Molecule)value).getName());
	return c != null ? c.toString() : null;
    }

    @Override
    public void applyState () throws InvalidStateException {
	MolProp[] props = getProperties ();
	logger.info(props.length+" properties selected!");
	if (props.length > 0) {
	    String[] p = new String[props.length];
	    for (int i = 0; i < props.length; ++i) {
		p[i] = props[i].name;
	    }
	    getModel().put("properties", p);
	}
    }

    public static void main (String[] argv) throws Exception {
	JFrame f = new JFrame ();
	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	StepGenerate sg = new StepGenerate ();
	f.getContentPane().add(sg);
	f.setSize(new Dimension (400, 400));
	f.pack();
	f.setVisible(true);
    }
}
