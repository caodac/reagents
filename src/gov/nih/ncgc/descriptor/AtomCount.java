// $Id: AtomCount.java 2464 2009-03-03 18:12:59Z nguyenda $

package gov.nih.ncgc.descriptor;

import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.formats.MolFormatException;
import chemaxon.util.MolHandler;
import chemaxon.sss.search.MolSearch;
import chemaxon.sss.search.SearchException;

public class AtomCount {
    static private Molecule _hba, _hbd, _rb;

    private int[] _counts = null;
    private Molecule target;

    public AtomCount (Molecule target) {
	if (target == null) {
	    throw new IllegalArgumentException ("Input molecule is null");
	}
	this.target = target;
    }

    static protected synchronized Molecule getHbaQuery () {
	if (_hba == null) {
	    try {
		_hba = new MolHandler ("[#8&!$(*~N~[OD1]),#7&H0;!$([D4]);!$([D3]-*=,:[#7,#8,#15,#16])]", true).getMolecule();
	    }
	    catch (MolFormatException ex) {
		ex.printStackTrace();
	    }
	}
	return _hba;
    }

    static protected synchronized Molecule getHbdQuery () {
	if (_hbd == null) {
	    try {
		_hbd = new MolHandler ("[#7,#8;H]", true).getMolecule();
	    }
	    catch (MolFormatException ex) {
		ex.printStackTrace();
	    }
	}
	return _hbd;
    }

    static protected synchronized Molecule getRbQuery () {
	if (_rb == null) {
	    try {
		_rb = new MolHandler 
		    ("[!$([NH]!@C(=O))&!D1&!$(*#*)]-&!@[!$([NH]!@C(=O))&!D1&!$(*#*)]", true).getMolecule();
	    }
	    catch (MolFormatException ex) {
		ex.printStackTrace();
	    }
	}
	return _rb;
    }

    public int[] getCount () {
	if (_counts == null) {
	    _counts = new int[155];
	    for (MolAtom a : target.getAtomArray()) {
		++_counts[a.getAtno()];
	    }
	}
	return _counts;
    }

    public int getCount (int z) {
	return getCount()[z];
    }

    protected int count (Molecule query) {
	int cnt = 0;
	try {
	    MolSearch ms = new MolSearch ();
	    ms.setTarget(target);
	    ms.setQuery(query);
	    int[][] hits = ms.findAll();
	    cnt =  hits != null ? hits.length : 0;
	}
	catch (SearchException ex) {
	    ex.printStackTrace();
	}
	return cnt;
    }

    public int hbaCount () { 
	return count (getHbaQuery ());
    }
    public int hbdCount () {
	return count (getHbdQuery ());
    }
    public int rbCount () {
	return count (getRbQuery ());
    }

    public static void main (String[] argv) throws Exception {
	if (argv.length < 1) {
	    System.out.println("usage: AtomCount FILES...");
	    System.exit(1);
	}
	
	for (int i = 0; i < argv.length; ++i) {
	    chemaxon.formats.MolImporter mi = 
		new chemaxon.formats.MolImporter (argv[i]);
	    for (Molecule mol = new Molecule (); mi.read(mol); ) {
		AtomCount ac = new AtomCount (mol);
		System.out.println(mol.getName() + " hba=" + ac.hbaCount() + " hbd=" + ac.hbdCount() + " rb=" + ac.rbCount() + " nO="+ac.getCount(8) + " nN="+ac.getCount(7));
	    }
	    mi.close();
	}
    }
}
