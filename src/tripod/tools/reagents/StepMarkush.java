package tripod.tools.reagents;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import chemaxon.struc.*;
import chemaxon.marvin.beans.MViewPane;
import chemaxon.marvin.beans.MSketchPane;
import chemaxon.marvin.common.UserSettings;


public class StepMarkush extends StepCommon {
    private static final Logger logger = Logger.getLogger
	(StepMarkush.class.getName());

    public static final String NAME = "Markush Structure";
    public static final String DESC = 
	"Please define your starting material as a Markush structure. "
	+"Be sure clearly annotate your R-group labels.";

    private MSketchPane sketcher;

    public StepMarkush () {
	super (NAME, DESC);
    }

    @Override
    protected void initUI () {
	sketcher = new MSketchPane ();
	sketcher.setCloseEnabled(false);
	setLayout (new BorderLayout ());
	add (sketcher);
    }

    @Override
    public void prepare () {
	Molecule core = (Molecule) model.get("core");
	sketcher.setMol(core);
    }

    @Override
    public void applyState () throws InvalidStateException {
	Molecule mol = sketcher.getMol();
	mol.hydrogenize(false);
	mol.expandSgroups();
	mol.setName("Core");

	logger.info("molecule: "+mol);

	MolAtom[] atoms = mol.getAtomArray();
	if (atoms.length == 0) {
	    throw new InvalidStateException
		("Please specify a Markush structure!");
	}

	java.util.List<MolAtom> rgroups = new ArrayList<MolAtom>();
	java.util.List<MolAtom> lists = new ArrayList<MolAtom>();
	for (MolAtom a : atoms) {
	    int atno = a.getAtno();
	    switch (atno) {
	    case MolAtom.RGROUP:
		rgroups.add(a);
		break;
	    case MolAtom.LIST:
		lists.add(a);
		break;
	    case MolAtom.PSEUDO:
	    case MolAtom.NOTLIST:
	    case MolAtom.ANY:
	    case MolAtom.HETERO:
	    case MolAtom.LP:
		throw new InvalidStateException 
		    ("Structure contains unsupported query atom: "+atno);
	    }
	}

	if (rgroups.isEmpty() && lists.isEmpty()) {
	    throw new InvalidStateException ("Not a Markush structure!");
	}

	if (rgroups.isEmpty()) {
	    rgroups = null; // remove
	}
	model.put("rgroups", rgroups);

	if (lists.isEmpty()) {
	    lists = null;
	}
	model.put("lists", lists);

	logger.info("Core: "+mol.toFormat("cxsmarts"));
	model.put("core", mol);
    }
}
