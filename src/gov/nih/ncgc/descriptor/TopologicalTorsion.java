// $Id: TopologicalTorsion.java 2468 2009-03-05 14:53:48Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.util.BitSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Vector;
import java.util.Stack;
import java.util.Arrays;

import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.formats.MolImporter;

public class TopologicalTorsion extends AtomPair {
    
    protected Molecule mol;
    protected Map<BitSet, int[]> torsions = new HashMap<BitSet, int[]>();

    protected TopologicalTorsion () {}
    public TopologicalTorsion (Molecule mol) {
	this.mol = mol;
	
	generateTorsions ();

	for (int[] tor : torsions.values()) {
	    int a = encode (tor[0], true);
	    int b = encode (tor[1], false);
	    int c = encode (tor[2], false);
	    int d = encode (tor[3], true);

	    // generate canonical ordering to prevent two torsions that are
	    //  in reverse order of one another
	    if (a > d) { // reverse the order
		a ^= d; d ^= a; a ^= d;
		b ^= c; c ^= b; b ^= c;
	    }

	    String tt = decode (a) 
		+ "_" + decode (b)
		+ "_" + decode (c)
		+ "_" + decode (d);
	    Integer count = sparseVector.get(tt);
	    sparseVector.put(tt, count != null ? count+1 : 1);
	}
    }

    protected void generateTorsions () {
	Stack<MolAtom> stack = new Stack<MolAtom>();
	for (MolAtom a : mol.getAtomArray()) {
	    stack.clear();
	    dfs (a, stack, 4);
	}
    }

    protected int encode (int a, boolean isTerminal) {
	MolAtom atom = mol.getAtom(a);
	int atype = 0xff;
	switch (atom.getAtno()) {
	case 6:
	case 7:
	case 8:
	case 9:
	case 15:
	case 16:
	case 17:
	case 35:
	case 53:
	    atype = atom.getAtno();
	    break;
	}
	int pi = numPiElectrons (atom);
	int nb = atom.getBondCount() - (isTerminal ? 1 : 2);

	// the order is different from the paper.. 
	//   where type & pi are swapped
	return (atype << 16) | (pi << 8) | nb;
    }

    protected void dfs (MolAtom node, Stack<MolAtom> path, int max) {
	path.push(node);

	// this search will generate some overlapping torsions... we're
	//   relying on the BitSet's hash to keep a set of unique torsions.
	if (path.size() == max) {
	    BitSet bs = new BitSet ();

	    int tt[] = new int[max], k = 0;
	    for (MolAtom a : path) {
		tt[k] = mol.indexOf(a);
		bs.set(tt[k]);
		++k;
	    }

	    if (!torsions.containsKey(bs)) {
		torsions.put(bs, tt);
		/*
		System.out.print(bs + " => ["+(tt[0]+1));
		for (int i = 1; i < tt.length; ++i) {
		    System.out.print(", " + (tt[i]+1));
		}
		System.out.println("]");
		*/
	    }
	}
	else {
	    for (int i = 0; i < node.getBondCount(); ++i) {
		MolAtom n = node.getBond(i).getOtherAtom(node);
		if (path.indexOf(n) < 0) {
		    dfs (n, path, max);
		}
	    }
	}

	path.pop();
    }

    public int[][] getTorsions () {
	int[][] tt = new int[torsions.size()][];
	int i = 0;
	for (int[] tor : torsions.values()) {
	    tt[i++] = tor;
	}
	return tt;
    }

    public static void main (String[] argv) throws Exception {
	System.setProperty("ap.driver", TopologicalTorsion.class.getName());
	AtomPair.main(argv);
    }
}
