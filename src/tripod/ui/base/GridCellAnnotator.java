// $Id: GridCellAnnotator.java 3286 2009-09-28 16:35:59Z nguyenda $

package tripod.ui.base;

public interface GridCellAnnotator {
    public String getGridCellAnnotation (Grid g, Object value, int cell);
    public String getGridCellBadgeAnnotation (Grid g, Object value, int cell);
}
