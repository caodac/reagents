// $Id: AtomTyper.java 2461 2009-03-01 23:12:12Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.util.Vector;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;

import chemaxon.util.MolHandler;
import chemaxon.sss.search.MolSearch;
import chemaxon.sss.search.SearchException;
import chemaxon.struc.Molecule;
import chemaxon.formats.MolFormatException;
import chemaxon.formats.MolImporter;

public class AtomTyper {
    
    static class Rule {
	String pattern;
	String value;
	Molecule query;

	public Rule (String pattern, String value) throws MolFormatException {
	    this.pattern = pattern;
	    this.value = value;
	    MolHandler mh = new MolHandler (pattern, true);
	    query = mh.getMolecule();
	}
	public String getPattern () { return pattern; }
	public String getValue () { return value; }
	public Molecule getQuery () { return query; }
    }

    protected Vector<Rule> rules = new Vector<Rule>();

    public AtomTyper () {
    }

    public AtomTyper (String file) throws IOException {
	load (file);
    }
    public AtomTyper (InputStream is) throws IOException {
	load (is);
    }

    public void load (String file) throws IOException {
	load (new FileInputStream (file));
    }

    public void load (InputStream is) throws IOException {
	BufferedReader br = new BufferedReader (new InputStreamReader (is));

	int cnt = 1;
	rules.clear();
	for (String line; (line = br.readLine()) != null; ++cnt) {
	    int pos = line.indexOf('#');
	    if (pos == 0) {
		continue;
	    }
	    line = line.trim();

	    String[] toks = line.split("[\\s]+");
	    if (toks.length > 1) {
		try {
		    rules.add(new Rule (toks[0], toks[1]));
		}
		catch (MolFormatException ex) {
		    System.err.println
			("** warning: bogus pattern at line " + cnt + ": " 
			 + toks[0]);
		}
	    }
	}
	//System.err.println(rules.size() + " rule(s) loaded!");
    }

    public String[] apply (Molecule target) {
	String[] types = new String[target.getAtomCount()];

	target.aromatize();
	MolSearch ms = new MolSearch ();
	ms.setHCountMatching(MolSearch.HCOUNT_MATCHING_EQUAL);
	ms.setOption(MolSearch.OPTION_ISOTOPE_MATCHING, 
		     MolSearch.ISOTOPE_MATCHING_EXACT);
	/*
	ms.setOption(MolSearch.OPTION_CHARGE_MATCHING,
		     MolSearch.CHARGE_MATCHING_EXACT);
	*/
	ms.setTarget(target);
	for (Rule r : rules) {
	    ms.setQuery(r.getQuery());
	    try {
		int[][] hits = ms.findAll();
		/*
		System.out.println(r.getPattern() + " " 
				   + r.getQuery().toFormat("smarts") + " " 
				   + (hits != null ? hits.length : 0) 
				   + " hit(s)");
		*/
		if (hits != null) {
		    for (int i = 0; i < hits.length; ++i) {
			int atom = hits[i][0];
			if (atom >= 0) {
			    // ignore explicit H matching implicit H in target
			    types[atom] = r.getValue();
			}
		    }
		}
	    }
	    catch (SearchException ex) {
		ex.printStackTrace();
	    }
	}

	return types;
    }

    public static void main (String[] argv) throws Exception {
	if (argv.length < 2) {
	    System.out.println("usage: AtomType RULES FILES...");
	    System.exit(1);
	}

	AtomTyper typer = new AtomTyper (argv[0]);
	for (int i = 1; i < argv.length; ++i) {
	    MolImporter mi = new MolImporter (argv[i]);
	    for (Molecule mol = new Molecule (); mi.read(mol); ) {
		System.out.println(mol.getName());
		String[] types = typer.apply(mol);
		for (int j = 0; j < types.length; ++j) {
		    System.out.printf(" Atom %1$2d: %2$s\n", j+1, types[j]);
		}
	    }
	    mi.close();
	}
    }
}
