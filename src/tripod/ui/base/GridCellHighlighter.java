
package tripod.ui.base;

import java.awt.Color;

public interface GridCellHighlighter {
    public Color getCellColor (Grid g, Object value, int cell);
}
