package tripod.tools.reagents;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingworker.SwingWorker;
import com.jidesoft.swing.JideTabbedPane;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import chemaxon.struc.*;
import chemaxon.formats.MolImporter;
import chemaxon.marvin.beans.MViewPane;
import chemaxon.marvin.beans.MSketchPane;
import chemaxon.marvin.common.UserSettings;
import chemaxon.sss.search.MolSearch;

import tripod.ui.base.*;


public class StepReagents extends StepCommon 
    implements GridCellAnnotator, ActionListener, ChangeListener {
    private static final Logger logger = Logger.getLogger
	(StepReagents.class.getName());

    public static final String NAME = "Reagent Specification";
    public static final String DESC = "Please specify the reagents for each "
	+"R-group position.  The reagents can either be loaded from a file "
	+"or manually added.";

    static final FileFilter FILE_FILTER = new FileNameExtensionFilter 
	("Molecular file formats (.sdf, .smi, .mol)", 
	 "sd", "sdf", "mol", "smi");


    class ReagentLoader extends SwingWorker<Throwable, Molecule> {
	InputStream is;
	Grid grid;
	Molecule material; // starting material
	int count = 0;

	ReagentLoader (InputStream is, Grid grid, Molecule material) {
	    this.is = is;
	    this.grid = grid;
	    this.material = material;
	}

	@Override
	protected Throwable doInBackground () {
	    try {
		MolImporter mi = new MolImporter (is);
		if (material != null) {
		    MolSearch ms = new MolSearch ();
		    ms.setQuery(material);
		    for (Molecule m; (m = mi.read()) != null; ) {
			m.hydrogenize(false);
			m.expandSgroups();
			m.aromatize();

			MolAtom[] atoms = m.getAtomArray();
			for (MolAtom a : atoms) {
			    a.setAtomMap(0);
			    a.setExtraLabel(null);
			}

			ms.setTarget(m);
			int[][] hits = ms.findAll();
			m.dearomatize();

			if (hits != null) {
			    for (int[] h : hits) {
				for (int i = 0; i < h.length; ++i) {
				    if (h[i] >= 0) {
					MolAtom ta = atoms[h[i]];
					MolAtom qa = material.getAtom(i);
					if (qa.getAtno() == MolAtom.RGROUP) {
					    ta.setAttach(MolAtom.ATTACH1);
					}
					ta.setAtomMap(i+1);
				    }
				}
			    }

			    publish (m);
			    ++count;
			}
			else {
			    logger.warning
				("Reagent "+m.getName()
				 +" doesn't contain the specified starting"
				 +" material "+material.toFormat("cxsmarts")
				 +"; reagent not added!");
			}
		    }		    
		}
		else {
		    for (Molecule m; (m = mi.read()) != null; ) {
			m.hydrogenize(false);
			m.expandSgroups();
			publish (m);
			++count;
		    }
		}
		mi.close();
	    }
	    catch (Exception ex) {
		return ex;
	    }
	    return null;
	}

	@Override
	protected void process (Molecule... mols) {
	    for (Molecule m : mols) {
		status.setText("Loading "+m.getName()+"...");
		grid.addValue(m);
	    }
	}
	
	@Override
	protected void done () {
	    status.setText(count+" reagent"+(count!=1?"s":"")+" added");
	    javax.swing.Timer timer = new javax.swing.Timer
		(5000, new ActionListener () {
			public void actionPerformed (ActionEvent e) {
			    updateStatus ();
			}
		    });
	    timer.setRepeats(false);
	    timer.start();
	    try {
		Throwable t = get ();
		if (t != null) {
		    JXErrorPane.showDialog(t);
		}
		else {
		}
	    }
	    catch (Exception ex) {
		JXErrorPane.showDialog(ex);
	    }
	}
    }

    // R-group and its reagents
    private Map<String, Grid> reagents = new TreeMap<String, Grid>();
    private JTabbedPane tabbed;
    private JLabel status;
    private JSlider slider;
    private JPopupMenu popup;
    private MSketchPane sketcher;
    private java.util.List<MolAtom> rgroups;
    private Map<Integer, Molecule> materials;
    private Map<String, Integer> rlabels = new HashMap<String, Integer>();

    public StepReagents () {
	super (NAME, DESC);
    }

    @Override
    protected void initUI () {
	status = new JLabel ();
	JPanel sp = new JPanel (new BorderLayout (2, 0));
	sp.add(status);

	Box box = Box.createHorizontalBox();
	JPanel bp = new JPanel (new GridLayout (1, 3, 2, 0));
	JButton btn;
	bp.add(btn = new JButton ("Load"));
	btn.setToolTipText("Load reagents from file");
	btn.addActionListener(this);
	
	bp.add(btn = new JButton ("Add"));
	btn.setToolTipText("Manually add a new reagent");
	btn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    showView ("add");
		}
	    });

	bp.add(btn = new JButton ("Clear"));
	btn.setToolTipText("Clear reagents");
	btn.addActionListener(this);

	slider = new JSlider (80, 300, 100);
	slider.addChangeListener(this);

	box.add(slider);
	box.add(Box.createHorizontalStrut(5));
	box.add(bp);
	sp.add(box, BorderLayout.EAST);

	tabbed = new JideTabbedPane ();
	tabbed.addChangeListener(new ChangeListener () {
		public void stateChanged (ChangeEvent e) {
		    updateStatus ();
		}
	    });

	popup = createPopup ();
	setLayout (new CardLayout ());

	JPanel pane = new JPanel (new BorderLayout (0, 5));
	pane.add(tabbed);
	pane.add(sp, BorderLayout.SOUTH);
	add (pane, "main"); // main view

	sketcher = new MSketchPane ();
	sketcher.setCloseEnabled(false);
	pane = new JPanel (new BorderLayout (0, 5));
	pane.add(sketcher);
	bp = new JPanel (new GridLayout (1, 2, 3, 0));
	bp.add(btn = new JButton ("Add"));
	btn.addActionListener(this);
	bp.add(btn = new JButton ("Close"));
	btn.addActionListener(new ActionListener () {
		public void actionPerformed (ActionEvent e) {
		    showView ("main"); // return to main view
		}
	    });
	sp = new JPanel ();
	sp.add(bp);
	pane.add(sp, BorderLayout.SOUTH);
	
	add (pane, "add"); // add view
	showView ("main");
    }

    void showView (String view) {
	((CardLayout)getLayout()).show(this, view);
    }

    @Override
    public void prepare () {
	ReagentGeneratorModel model = getModel ();
	rgroups = (java.util.List)model.get("rgroups");

	materials = (Map)model.get("materials");

	rlabels.clear(); 
	Set<String> tabs = new HashSet<String>();
	for (MolAtom a : rgroups) {
	    int rg = a.getRgroup();
		
	    String name = getRLabel (rg);
	    rlabels.put(name, rg);
		
	    Grid grid = reagents.get(name);
	    if (grid == null) {
		reagents.put(name, grid = createGrid ());
	    }
		
	    // find the tab that corresponds to this r-group
	    int index = -1;
	    for (int i = 0; i < tabbed.getTabCount(); ++i) {
		String title = tabbed.getTitleAt(i);
		if (title.equals(name)) {
		    index = i;
		    break;
		}
	    }
		
	    if (index < 0) { // new tab
		tabbed.addTab(name, new JScrollPane (grid));
	    }
	    tabs.add(name);
	}
	    
	// now remove any tab that isn't in tabs
	for (int i = 0; i < tabbed.getTabCount(); ++i) {
	    String name = tabbed.getTitleAt(i);
	    if (!tabs.contains(name)) {
		tabbed.removeTabAt(i);
	    }
	}
	    
	Map<Integer, Molecule[]> rea = (Map) model.get("reagents");
	if (rea != null) {
	    for (Map.Entry<Integer, Molecule[]> me : rea.entrySet()) {
		String name = getRLabel (me.getKey());
		Grid g = reagents.get(name);
		g.clear();
		for (Molecule m : me.getValue()) {
		    g.addValue(m);
		}
	    }
	    updateStatus ();
	}
	else {
	    for (Grid g : reagents.values()) {
		g.clear();
	    }
	}
    }

    void updateStatus () {
	int index = tabbed.getSelectedIndex();
	if (index >= 0) {
	    String tab = tabbed.getTitleAt(index);
	    Grid g = reagents.get(tab);
	    if (g != null) {
		int cnt = g.getCellCount();
		status.setText(tab+" has "+cnt
			       +" reagent"+(cnt!=1?"s":"")+" specified");
	    }
	}
    }

    JPopupMenu createPopup () {
	JPopupMenu popup = new JPopupMenu ();
	JMenuItem item = popup.add("Delete");
	item.setToolTipText("Delete selected reagents");
	item.addActionListener(this);

	return popup;
    }


    Grid createGrid () {
	Grid grid = new Grid ();
	grid.setCellAnnotator(this);
	MolCellEditor mce = new MolCellEditor ();
	mce.setAtomMapVisible(true);
	grid.setCellEditor(mce);
	MolCellRenderer mcr = new MolCellRenderer ();
	mcr.setAtomMapVisible(true);
	grid.setCellRenderer(mcr);
	grid.setCellSize(slider.getValue());
	grid.setPopupMenu(popup);
	return grid;
    }

    public void actionPerformed (ActionEvent e) {
	String cmd = e.getActionCommand();
	if ("load".equalsIgnoreCase(cmd)) {
	    doLoad ();
	}
	else if ("add".equalsIgnoreCase(cmd)) {
	    doAdd ();
	}
	else if ("clear".equalsIgnoreCase(cmd)) {
	    doClear ();
	}
	else if ("delete".equalsIgnoreCase(cmd)) {
	    doDelete ();
	}
    }

    Grid getSelectedGrid () {
	return reagents.get
	    (tabbed.getTitleAt(tabbed.getSelectedIndex()));
    }

    Molecule getSelectedMaterial () {
	if (materials != null) {
	    String label = tabbed.getTitleAt(tabbed.getSelectedIndex());
	    Integer rg = rlabels.get(label);
	    return materials.get(rg);
	}
	return null;
    }

    void doLoad () {
	/*
	JFileChooser chooser = getFileChooser ();
	chooser.setDialogTitle("Load reagents for "
			       +tabbed.getTitleAt(tabbed.getSelectedIndex()));
	chooser.resetChoosableFileFilters();
	chooser.setFileFilter(FILE_FILTER);
	if (JFileChooser.APPROVE_OPTION == chooser.showOpenDialog(this)) {
	    File file = chooser.getSelectedFile();
	    loadReagents (file);
	}
	*/
	try {
	    loadReagents (getInputFileStream 
			  ("Load reagents for "
			   +tabbed.getTitleAt(tabbed.getSelectedIndex()),
			   "sd", "sdf", "mol", "smi"));
	}
	catch (IOException ex) {
	    JXErrorPane.showDialog(ex);
	}
    }

    void doAdd () {
	Molecule mol = sketcher.getMol();
	boolean ok = false;
	if (mol.getAtomCount() == 0) {
	    JOptionPane.showMessageDialog
		(this, "Nothing to add!", "Error", JOptionPane.ERROR_MESSAGE);
	}
	else if (mol.isQuery()) {
	    JOptionPane.showMessageDialog
		(this, "Reagent is a Markush structure!", 
		 "Error", JOptionPane.ERROR_MESSAGE);
	}
	else {
	    mol = ((RgMolecule)mol).getRoot();
	    Molecule material = getSelectedMaterial ();
	    if (material != null) {
		MolSearch ms = new MolSearch ();
		ms.setQuery(material);

		mol.aromatize();
		ms.setTarget(mol);
		try {
		    int[][] hits = ms.findAll();
		    mol.dearomatize();

		    if (hits != null) {
			for (int[] h : hits) {
			    for (int i = 0; i < h.length; ++i) {
				if (h[i] >= 0) {
				    mol.getAtom(h[i]).setAtomMap(i+1);
				}
			    }
			}
			ok = true;
		    }
		    else {
			JOptionPane.showMessageDialog
			    (this, "Reagent doesn't have specified "
			     +"starting material!", "Error", 
			     JOptionPane.ERROR_MESSAGE);
		    }
		}
		catch (Exception ex) {
		    JXErrorPane.showDialog(ex);
		}
	    }
	    else {
		ok = true;
	    }
	}

	if (!ok) return;

	Grid grid = getSelectedGrid ();
	grid.addValue(mol);
	updateStatus ();
	//showView ("load"); // return to the load view
    }

    void doDelete () {
	Grid grid = getSelectedGrid ();
	if (grid != null) {
	    grid.removeSelectedValues();
	    updateStatus ();
	}
    }

    void doClear () {
	Grid grid = getSelectedGrid ();
	if (grid != null) {
	    grid.clear();
	    updateStatus ();
	}
    }

    public void stateChanged (ChangeEvent e) {
	int size = slider.getValue();
	for (Grid g : reagents.values()) {
	    g.setCellSize(size);
	}
    }

    void loadReagents (InputStream is) {
	Grid grid = getSelectedGrid ();
	if (grid != null) {
	    Molecule m = getSelectedMaterial ();
	    logger.info("Starting material for "
			+tabbed.getTitleAt(tabbed.getSelectedIndex())
			+": "+(m != null ? m.toFormat("cxsmarts") : "(none)"));
	    new ReagentLoader (is, grid, m).execute();
	}
	else {
	    logger.severe("No selected reagent tab!");
	}
    }

    public String getGridCellAnnotation (Grid g, Object v, int c) {
	Molecule m = (Molecule)v;
	if (m != null) {
	    return m.getName();
	}
	return null;
    }
    public String getGridCellBadgeAnnotation (Grid g, Object v, int c) {
	return null;
    }

    @Override
    public void applyState () throws InvalidStateException {
	for (int i = 0; i < tabbed.getTabCount(); ++i) {
	    String name = tabbed.getTitleAt(i);
	    Grid grid = reagents.get(name);
 	    if (grid.getCellCount() == 0) {
		throw new InvalidStateException 
		    ("No reagents specified for "+name+"!");
	    }
	}

	Map<Integer, Molecule[]> reagents = new HashMap<Integer, Molecule[]>();
	for (MolAtom a : rgroups) {
	    int rg = a.getRgroup();
	    if (!reagents.containsKey(rg)) {
		Grid grid = this.reagents.get(getRLabel (rg));
		Molecule[] mols = new Molecule[grid.getCellCount()];
		for (int i = 0; i < mols.length; ++i) {
		    mols[i] = (Molecule)grid.getValueAt(i);
		}
		reagents.put(rg, mols);
		logger.info("Reagent for R"+rg+": "+mols.length);
	    }
	}

	getModel().put("reagents", reagents);
    }
}

