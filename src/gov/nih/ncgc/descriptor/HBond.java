
package gov.nih.ncgc.descriptor;

import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.struc.MolBond;

/**
 * Calculate H-bond donor and acceptor for O and N based on Kubinyi's 
 * definitions at:
 * http://www.beilstein-institut.de/bozen2004/proceedings/Kubinyi/Kubinyi.htm
 * http://www.kubinyi.de/dd-18.pdf
 * http://www.daylight.com/dayhtml_tutorials/languages/smarts/smarts_examples.html
 */
public class HBond {
    /*
     * WARNING: the methods here assume that Molecule must already 
     *  be aromatized and Hs are implicitized!
     */
    public static int acceptors (Molecule mol) {
	int acc = 0;
	for (MolAtom a : mol.getAtomArray()) {
	    if (isAcceptor (a)) {
		++acc;
	    }
	}
	return acc;
    }

    public static int donors (Molecule mol) {
	int don = 0;
	for (MolAtom a : mol.getAtomArray()) {
	    if (isDonor (a)) {
		++don;
	    }
	}
	return don;
    }

    public static boolean isDonor (MolAtom atom) {
	int atno = atom.getAtno();
	if (atno == 7 || atno == 8 /*|| atno == 9*/) {
	    return atom.getImplicitHcount() > 0;
	}
	return false;
    }

    public static boolean isAcceptor (MolAtom atom) {
	int atno = atom.getAtno();
	switch (atno) {
	case 8:
	    { int sb = 0, db = 0; // single, double
		for (int i = 0; i < atom.getBondCount(); ++i) {
		    int type = atom.getBond(i).getType();
		    switch (type) {
		    case 1: ++sb; break;
		    case 2: ++db; break;
		    case MolBond.AROMATIC:
			return false;
		    }
		}

		if (db > 0 || atom.getCharge() < 0) {  // [O-] or =O
		    return true;
		}

		/* check to see if either of its neighboring atoms
		 * contains a double bond
		 */
		if (sb == 2) { // check for O(C)(C=*)
		    for (int i = 0; i < atom.getBondCount(); ++i) {
			MolAtom xa = atom.getBond(i).getOtherAtom(atom);
			for (int j = 0; j < xa.getBondCount(); ++j) {
			    if (xa.getBond(j).getType() == 2) {
				return false;
			    }
			}
		    }
		}
	    }
	    // fall through

	case 7:
	    {
		int h = atom.getImplicitHcount();
		if (h > 0 && atno == 8) { // OH is both acc & don
		    return true;
		}
		return h == 0;
	    }
	}
	return false;
    }
}
