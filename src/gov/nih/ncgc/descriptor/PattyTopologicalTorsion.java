// $Id: PattyTopologicalTorsion.java 2467 2009-03-05 14:53:09Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.util.Arrays;
import java.util.BitSet;
import chemaxon.struc.Molecule;

import static gov.nih.ncgc.descriptor.PattyTyper.Type;

public class PattyTopologicalTorsion extends TopologicalTorsion {
    protected PattyTopologicalTorsion () {}
    public PattyTopologicalTorsion (Molecule mol) {
	this.mol = mol;
	generateTorsions ();
	
	Type[] types = PattyTyper.getInstance().getTypes(mol);
	for (int[] tor : torsions.values()) {
	    int a = types[tor[0]].ordinal();
	    int b = types[tor[1]].ordinal();
	    int c = types[tor[2]].ordinal();
	    int d = types[tor[3]].ordinal();

	    // make sure we have a canonical torsion
	    if (a > d) {
		a ^= d; d ^= a; a ^= d;
		b ^= c; c ^= b; b ^= c;
	    }

	    String path = Type.getInstance(a) 
		+ "_" + Type.getInstance(b) 
		+ "_" + Type.getInstance(c)
		+ "_" + Type.getInstance(d);

	    Integer count = sparseVector.get(path);
	    sparseVector.put(path, count != null ? count+1 : 1);
	}
    }

    public static void main (String[] argv) throws Exception {
	System.setProperty
	    ("ap.driver", PattyTopologicalTorsion.class.getName());
	AtomPair.main(argv);
    }
}
