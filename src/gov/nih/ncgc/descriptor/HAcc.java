package gov.nih.ncgc.descriptor;

import java.io.InputStream;
import java.io.IOException;

public class HAcc extends GroupContrib {
    private static HAcc instance = null;
    
    private HAcc (InputStream is) throws IOException {
	super (is);
    }

    public synchronized static GroupContrib getInstance () {
	if (instance == null) {
	    try {
		instance = new HAcc
		    (HAcc.class.getResourceAsStream("resources/hacc.txt"));
	    }
	    catch (IOException ex) {
		ex.printStackTrace();
	    }
	}
	return instance;
    }
}
