package tripod.tools.reagents;

import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.formats.MolImporter;

public class ReagentGeneratorModel extends DynamicModel  {
    private static final Logger logger = 
	Logger.getLogger(ReagentGeneratorModel.class.getName());

    static class HasRg implements Condition {
	HasRg () {
	}

	public boolean evaluate (WizardModel model) {
	    ReagentGeneratorModel rgm = (ReagentGeneratorModel)model;
	    boolean hasRg = rgm.contains("rgroups");
	    if (!hasRg) { // side effects
		rgm.put("materials", null);
		rgm.put("reagents", null);
	    }
	    return hasRg;
	}
    }

    private Hashtable props = new Hashtable ();

    public ReagentGeneratorModel () {
	HasRg hasRg = new HasRg ();
	add (new StepWelcome ());
	add (new StepMarkush ());
	add (new StepStartingMaterial (), hasRg);
	add (new StepReagents (), hasRg);
	add (new StepGenerate ());
	setLastVisible (false);
    }

    public boolean contains (String key) {
	return props.containsKey(key);
    }

    public Object put (String key, Object value) {
	if (value == null) {
	    value = props.remove(key);
	}
	else {
	    value = props.put(key, value);
	}
	return value;
    }
    public Object get (String key) {
	return props.get(key);
    }

    public void save (File file) throws IOException {
	logger.info("Saving project to file "+file);
	save (new FileOutputStream (file));
    }

    public void clear () { 
	props.clear();
    }

    public void save (OutputStream os) throws IOException {
	ZipOutputStream zos = new ZipOutputStream (os);
	Molecule core = (Molecule) get ("core");
	if (core == null) {
	    throw new IllegalStateException ("No core defined!");
	}
	ZipEntry ze = new ZipEntry ("core.mrv");
	zos.putNextEntry(ze);
	zos.write(core.toFormat("mrv").getBytes("utf-8"));
	zos.closeEntry();
	
	java.util.List<MolAtom> rgroups = (java.util.List) get ("rgroups");
	if (rgroups != null) {
	    Map<Integer, Molecule> materials = (Map) get ("materials");
	    if (materials == null) {
		throw new IllegalArgumentException ("No materials defined!");
	    }

	    for (Map.Entry<Integer, Molecule> me : materials.entrySet()) {
		ze = new ZipEntry ("R"+me.getKey()+".mrv");
		zos.putNextEntry(ze);
		zos.write(me.getValue().toFormat("mrv").getBytes("utf-8"));
		zos.closeEntry();
	    }
	    
	    Map<Integer, Molecule[]> reagents = (Map) get ("reagents");
	    if (reagents == null) {
		throw new IllegalStateException ("No reagents defined!");
	    }

	    for (Map.Entry<Integer, Molecule[]> me : reagents.entrySet()) {
		String path = "R"+me.getKey()+"/";
		Molecule[] rea = me.getValue();
		for (int i = 0; i < rea.length; ++i) {
		    ze = new ZipEntry (path+String.format("%1$05d.mrv", i+1));
		    zos.putNextEntry(ze);
		    zos.write(rea[i].toFormat("mrv").getBytes("utf-8"));
		    zos.closeEntry();
		}
	    }
	}

	String[] props = (String[]) get ("properties");
	if (props != null) {
	    
	}
	zos.close();
    }

    public void load (InputStream is) throws IOException {
	ZipInputStream zis = new ZipInputStream (is);

	java.util.List<MolAtom> rgroups = new ArrayList<MolAtom>();
	java.util.List<MolAtom> lists = new ArrayList<MolAtom>();

	Map<Integer, Molecule> materials = new TreeMap<Integer, Molecule>();
	Map<Integer, List<Molecule>> reagents = 
	    new HashMap<Integer, List<Molecule>>();

	for (ZipEntry ze; (ze = zis.getNextEntry()) != null; ) {
	    if (ze.isDirectory()) {
		// do nothing
	    }
	    else if ("core.mrv".equals(ze.getName())) {
		Molecule core = new MolImporter (zis).read();
		for (MolAtom a : core.getAtomArray()) {
		    int atno = a.getAtno();
		    switch (atno) {
		    case MolAtom.RGROUP:
			rgroups.add(a);
			break;
		    case MolAtom.LIST:
			lists.add(a);
			break;
		    case MolAtom.PSEUDO:
		    case MolAtom.NOTLIST:
		    case MolAtom.ANY:
		    case MolAtom.HETERO:
		    case MolAtom.LP:
			throw new IllegalStateException 
			    ("Structure contains unsupported query atom: "
			     +atno);
		    }
		}

		if (!rgroups.isEmpty()) {
		    put ("rgroups", rgroups);
		}
		
		if (!lists.isEmpty()) {
		    put ("lists", lists);
		}

		put ("core", core);
		zis.closeEntry();
	    }
	    else {
		String name = ze.getName();
		if (name.charAt(0) == 'R') {
		    try {
			int pos = name.lastIndexOf('/');
			if (pos > 0) {
			    int Rg = Integer.parseInt(name.substring(1, pos));
			    List<Molecule> rea = reagents.get(Rg);
			    if (rea == null) {
				reagents.put
				    (Rg, rea = new ArrayList<Molecule>());
			    }
			    rea.add(new MolImporter (zis).read());
			}
			else if ((pos = name.lastIndexOf('.')) > 0) {
			    int Rg = Integer.parseInt(name.substring(1, pos));
			    materials.put(Rg, new MolImporter (zis).read());
			}
		    }
		    catch (Exception ex) {
			// not what we're looking for
		    }
		}
	    }
	}

	logger.info(materials.size()+" reagents!");
	if (!materials.isEmpty()) {
	    put ("materials", materials);
	}
	
	if (reagents.size() != materials.size()) {
	    logger.warning("Starting materials and reagents don't match!");
	}

	Map<Integer, Molecule[]> rea = new TreeMap<Integer, Molecule[]>();
	for (Map.Entry<Integer, List<Molecule>> me : reagents.entrySet()) {
	    rea.put(me.getKey(), me.getValue().toArray(new Molecule[0]));
	}

	if (!rea.isEmpty()) {
	    put ("reagents", rea);
	}
    }

    public void load (File file) throws IOException {
	ZipFile zf = new ZipFile (file);
	props.clear();

	ZipEntry ze = zf.getEntry("core.mrv");
	if (ze == null) {
	    throw new IllegalArgumentException
		("Invalid zip file: can't find core.mrv");
	}

	Molecule core = new MolImporter (zf.getInputStream(ze)).read();
	put ("core", core);

	java.util.List<MolAtom> rgroups = new ArrayList<MolAtom>();
	java.util.List<MolAtom> lists = new ArrayList<MolAtom>();
	for (MolAtom a : core.getAtomArray()) {
	    int atno = a.getAtno();
	    switch (atno) {
	    case MolAtom.RGROUP:
		rgroups.add(a);
		break;
	    case MolAtom.LIST:
		lists.add(a);
		break;
	    case MolAtom.PSEUDO:
	    case MolAtom.NOTLIST:
	    case MolAtom.ANY:
	    case MolAtom.HETERO:
	    case MolAtom.LP:
		throw new IllegalStateException 
		    ("Structure contains unsupported query atom: "+atno);
	    }
	}

	if (!rgroups.isEmpty()) {
	    put ("rgroups", rgroups);
	}

	if (!lists.isEmpty()) {
	    put ("lists", lists);
	}

	Map<Integer, Molecule> materials = new TreeMap<Integer, Molecule>();
	Map<Integer, List<Molecule>> reagents = 
	    new HashMap<Integer, List<Molecule>>();
	for (Enumeration en = zf.entries(); en.hasMoreElements(); ) {
	    ze = (ZipEntry)en.nextElement();
	    if (ze.isDirectory()) 
		continue;

	    String name = ze.getName();
	    if (name.charAt(0) == 'R') {
		try {
		    int pos = name.lastIndexOf('/');
		    if (pos > 0) {
			int Rg = Integer.parseInt(name.substring(1, pos));
			List<Molecule> rea = reagents.get(Rg);
			if (rea == null) {
			    reagents.put(Rg, rea = new ArrayList<Molecule>());
			}
			rea.add(new MolImporter 
				(zf.getInputStream(ze)).read());
		    }
		    else if ((pos = name.lastIndexOf('.')) > 0) {
			int Rg = Integer.parseInt(name.substring(1, pos));
			materials.put(Rg, new MolImporter 
				      (zf.getInputStream(ze)).read());
		    }
		}
		catch (Exception ex) {
		    // not what we're looking for
		}
	    }
	}
	logger.info(file+": "+materials.size()+" reagents!");
	if (!materials.isEmpty()) {
	    put ("materials", materials);
	}
	
	if (reagents.size() != materials.size()) {
	    logger.warning("Starting materials and reagents don't match!");
	}

	Map<Integer, Molecule[]> rea = new TreeMap<Integer, Molecule[]>();
	for (Map.Entry<Integer, List<Molecule>> me : reagents.entrySet()) {
	    rea.put(me.getKey(), me.getValue().toArray(new Molecule[0]));
	}

	if (!rea.isEmpty()) {
	    put ("reagents", rea);
	}
    }
}
