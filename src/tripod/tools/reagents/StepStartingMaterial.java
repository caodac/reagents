package tripod.tools.reagents;

import java.util.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.io.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.jidesoft.swing.JideTabbedPane;
import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import chemaxon.struc.*;
import chemaxon.marvin.beans.MViewPane;
import chemaxon.marvin.beans.MSketchPane;
import chemaxon.marvin.common.UserSettings;


public class StepStartingMaterial extends StepCommon {
    private static final Logger logger = Logger.getLogger
	(StepMarkush.class.getName());

    public static final String NAME = "Starting Materials";
    public static final String DESC = 
	"Please specify the starting material for each R-group.";

    private Map<String, MSketchPane> materials = 
	new TreeMap<String, MSketchPane>();
    private JTabbedPane tabbed;
    private JLabel label;
    private java.util.List<MolAtom> rgroups;

    public StepStartingMaterial () {
	super (NAME, DESC);
    }

    @Override
    protected void initUI () {
	JPanel panel = new JPanel (new BorderLayout (0, 5));
	tabbed = new JideTabbedPane ();
	panel.add(tabbed);
	
	label = new JLabel ("");
	panel.add(label, BorderLayout.SOUTH);
	setLayout (new BorderLayout ());
	add (panel);
    }

    @Override
    public void prepare () {
	ReagentGeneratorModel model = getModel ();
	rgroups = (java.util.List<MolAtom>)model.get("rgroups");

	Set<String> tabs = new HashSet<String>();
	for (MolAtom a : rgroups) {
	    int rg = a.getRgroup();
	    String name = getRLabel (rg);
	    
	    MSketchPane sketcher = materials.get(name);
	    if (sketcher == null) {
		materials.put(name, sketcher = createSketcher ());
	    }
		
	    // find the tab that corresponds to this r-group
	    int index = -1; 
	    for (int i = 0; i < tabbed.getTabCount(); ++i) {
		String title = tabbed.getTitleAt(i);
		if (title.equals(name)) {
		    index = i;
		    break;
		}
	    }
		
	    if (index < 0) { // new tab
		tabbed.addTab(name, sketcher);
	    }
	    tabs.add(name);
	}

	Map<Integer, Molecule> mat = (Map) model.get("materials");
	if (mat != null) {
	    for (Map.Entry<Integer, Molecule> me : mat.entrySet()) {
		String name = getRLabel (me.getKey());
		MSketchPane sketch = materials.get(name);
		sketch.setMol(me.getValue());
	    }
	}
	else {
	    for (MSketchPane sketch : materials.values()) {
		sketch.setMol((Molecule)null);
	    }
	}

	// now remove any tab that isn't in tabs
	for (int i = 0; i < tabbed.getTabCount(); ++i) {
	    String name = tabbed.getTitleAt(i);
	    if (!tabs.contains(name)) {
		tabbed.removeTabAt(i);
	    }
	}
    }

    static MSketchPane createSketcher () {
	MSketchPane sketcher = new MSketchPane ();
	JMenuBar menu = sketcher.getJMenuBar();
	menu.removeAll();
	sketcher.setCloseEnabled(false);
	sketcher.setJMenuBar(null);
	return sketcher;
    }

    @Override
    public void applyState () throws InvalidStateException {
	Map<Integer, Molecule> materials = new TreeMap<Integer, Molecule>();

	for (MolAtom a : rgroups) {
	    int rg = a.getRgroup();
	    String name = getRLabel (rg);

	    if (!materials.containsKey(rg)) {
		Molecule mol = this.materials.get(name).getMol();
		mol.hydrogenize(false);
		mol.expandSgroups();
		mol.setName(name);

		if (mol.getAtomCount() == 0) {
		}
		else {
		    int r = 0;
		    for (MolAtom atom : mol.getAtomArray()) {
			if (atom.getAtno() == MolAtom.RGROUP) {
			    r = atom.getRgroup();
			}
		    }

		    if (r != rg) {
			for (int i = 0; i < tabbed.getTabCount(); ++i) {
			    if (name.equals(tabbed.getTitleAt(i))) {
				tabbed.setSelectedIndex(i);
			    }
			}

			if (r == 0) {
			    throw new InvalidStateException
				("Starting material for "+name+" doesn't "
				 +"have R-group annotated!");
			}
			else {
			    throw new InvalidStateException
				("Starting material for "+name
				 +" has incorrect R-group annotation!");
			}
		    }
		    else {
			materials.put(rg, mol);
			logger.info("Starting material for R"
				    +rg+": "+mol.toFormat("cxsmarts"));
		    }
		}
	    }
	}

	if (!materials.isEmpty()) {
	    getModel().put("materials", materials);
	}
    }
}
