package tripod.tools.reagents;

import java.util.*;
import java.io.*;

import javax.swing.JFileChooser;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jdesktop.swingx.JXErrorPane;
import com.jgoodies.looks.plastic.PlasticFileChooserUI;

import org.pietschy.wizard.*;
import org.pietschy.wizard.models.*;

import chemaxon.struc.Molecule;
import javax.jnlp.*;

public class StepCommon extends PanelWizardStep {
    static public boolean JNLP = false;
    static final ImageIcon logo = new ImageIcon 
	(StepCommon.class.getResource("resources/logo_ncgc.gif"));

    protected JFileChooser fileChooser;
    protected ReagentGeneratorModel model;

    protected StepCommon (String name, String desc) {
	this (name, desc, logo);
    }

    protected StepCommon (String name, String desc, Icon icon) {
	super (name, desc, icon);
	initUI ();
	setComplete (true);
    }

    // Override by subclasses to provide meaningful UI
    protected void initUI () {
    }

    protected JFileChooser getFileChooser () {
	if (fileChooser == null) {
	    fileChooser = new JFileChooser 
		("."/*System.getProperty("user.home")*/);
	    new PlasticFileChooserUI (fileChooser);
	}

	return fileChooser;
    }

    InputStream getInputFileStream (String mesg, String... filters) 
	throws IOException {
	InputStream is = null;

	if (JNLP) {
	    try {
		FileOpenService fos = (FileOpenService)ServiceManager.lookup
		    ("javax.jnlp.FileOpenService");
		FileContents file = fos.openFileDialog(".", filters);
		is = file.getInputStream();
	    }
	    catch (Exception ex) {
		JXErrorPane.showDialog(ex);
	    }
	}
	else {
	    JFileChooser chooser = getFileChooser ();
	    chooser.setDialogTitle(mesg);

	    if (filters != null && filters.length > 0) {
		chooser.resetChoosableFileFilters();
		chooser.setFileFilter
		    (new FileNameExtensionFilter 
		     (getFileFormatConcat (filters), filters));
	    }

	    if (JFileChooser.APPROVE_OPTION == chooser.showOpenDialog(this)) {
		is = new FileInputStream (chooser.getSelectedFile());
	    }
	}

	return is;
    }

    String getFileFormatConcat (String... filters) {
	if (filters == null || filters.length == 0) {
	    return "";
	}

	StringBuilder sb = new StringBuilder ("File formats");
	sb.append(" (."+filters[0]);
	for (int i = 1; i < filters.length; ++i) {
	    sb.append(", ."+filters[i]);
	}
	sb.append(")");

	return sb.toString();
    }

    OutputStream getOutputFileStream (String mesg, String... filters) 
	throws IOException {
	OutputStream os = null;

	if (JNLP) {
	    try {
		final FileSaveService fss = 
		    (FileSaveService)ServiceManager.lookup
		    ("javax.jnlp.FileSaveService");
		
		// write an empty byte to the file
		FileContents f = fss.saveFileDialog
		    (null, null, new ByteArrayInputStream
		     ("".getBytes("UTF-8")), null);
		if (f == null || !f.canWrite()) {
		    JOptionPane.showMessageDialog
			(this, "Can't open file for writing", "Error", 
			 JOptionPane.ERROR_MESSAGE);
		}
		else {
		    f.setMaxLength(20*1024*1024);// 20 mb max
		    os = f.getOutputStream(true);
		}
	    }
	    catch (Exception ex) {
		JXErrorPane.showDialog(ex);
	    }
	}
	else {
	    JFileChooser chooser = getFileChooser ();

	    chooser.setDialogTitle(mesg);
	    if (filters != null && filters.length > 0) {
		chooser.resetChoosableFileFilters();
		chooser.setFileFilter
		    (new FileNameExtensionFilter 
		     (getFileFormatConcat (filters), filters));
	    }

	    int op = chooser.showSaveDialog(this);
	    if (op == JFileChooser.APPROVE_OPTION) {
		File f = chooser.getSelectedFile();
		if (f.exists()) {
		    op = JOptionPane.showConfirmDialog
			(this, "File \""+f.getName()
			 +"\" exists; override?","Confirmation", 
			 JOptionPane.YES_NO_OPTION);
		    if (op == JOptionPane.YES_OPTION) {
			os = new FileOutputStream (f);
		    }
		    else {
			JOptionPane.showMessageDialog
			    (this, "File save canceled!", "Message", 
			     JOptionPane.INFORMATION_MESSAGE);
		    }
		}
		else {
		    os = new FileOutputStream (f);
		}
	    }
	    else {
		JOptionPane.showMessageDialog
		    (this, "File save canceled!", "Message", 
		     JOptionPane.INFORMATION_MESSAGE);
	    }
	}

	return os;
    }

    @Override
    public void init (WizardModel model) {
	this.model = (ReagentGeneratorModel)model;
    }

    protected ReagentGeneratorModel getModel () { return model; }

    protected static String getRLabel (int rg) {
	return "R"+rg;
    }

    static void addSeparator (JPanel pane, String title) {
	pane.add(new JLabel (title), 
		 "gapbottom 1, span, split 2, aligny center");
	pane.add(new JSeparator (), "gapleft rel,growx");	
    }
}
