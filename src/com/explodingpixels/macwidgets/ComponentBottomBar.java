package com.explodingpixels.macwidgets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.Border;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Color;

import com.explodingpixels.border.FocusStateMatteBorder;
import com.explodingpixels.painter.FocusStatePainter;
import com.explodingpixels.painter.GradientPainter;
import com.explodingpixels.painter.Painter;
import com.explodingpixels.util.PlatformUtils;
import com.explodingpixels.widgets.WindowDragger;
import com.explodingpixels.widgets.WindowUtils;

public class ComponentBottomBar extends TriAreaComponent {
    private static final Color ACTIVE_TOP_COLOR = new Color(0xcccccc);
    private static final Color ACTIVE_BOTTOM_COLOR = new Color(0xa7a7a7);
    private static final Color INACTIVE_TOP_COLOR = new Color(0xe9e9e9);
    private static final Color INACTIVE_BOTTOM_COLOR = new Color(0xd8d8d8);
    private static final Color BORDER_HIGHLIGHT_COLOR = new Color(255, 255, 255, 100);

    private static final Color LEOPARD_ACTIVE_TOP_COLOR = new Color(0xbbbbbb);
    private static final Color LEOPARD_ACTIVE_BOTTOM_COLOR = new Color(0x969696);
    private static final Color LEOPARD_INACTIVE_TOP_COLOR = new Color(0xe3e3e3);
    private static final Color LEOPARD_INACTIVE_BOTTOM_COLOR = new Color(0xcfcfcf);
    private static final Color LEOPARD_BORDER_HIGHLIGHT_COLOR = new Color(255, 255, 255, 110);


    ComponentBottomBar() {
        super();
	/*
        setBackgroundPainter(MacButtonFactory.GRADIENT_BUTTON_IMAGE_PAINTER);
        getComponent().setBorder(BorderFactory.createMatteBorder(1,0,0,0,
                MacButtonFactory.GRADIENT_BUTTON_BORDER_COLOR));
	*/
	createAndInstallBackgroundPainter ();
	createAndInstallBorder();	
    }

    public void addComponentToLeftWithBorder(JComponent toolToAdd) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setOpaque(false);
	/*
        panel.setBorder(BorderFactory.createMatteBorder(0,0,0,1,
                MacButtonFactory.GRADIENT_BUTTON_BORDER_COLOR));
	*/
        panel.add(toolToAdd, BorderLayout.CENTER);
        super.addComponentToLeft(panel);
    }

    public void addComponentToCenterWithBorder(JComponent toolToAdd) {
        // TODO use matteBorder when on first center item addition.
        // if this is the first component being added, add a line to the left
        //    and right of the component.
        // else add a border just to the right.
        Border matteBorder = getCenterComponentCount() == 0
                ? BorderFactory.createMatteBorder(0,1,0,1,
                MacButtonFactory.GRADIENT_BUTTON_BORDER_COLOR)
                : BorderFactory.createMatteBorder(0,0,0,1,
                MacButtonFactory.GRADIENT_BUTTON_BORDER_COLOR);

        JPanel panel = new JPanel(new BorderLayout());
        panel.setOpaque(false);
        panel.setBorder(BorderFactory.createMatteBorder(0,0,0,1,
                MacButtonFactory.GRADIENT_BUTTON_BORDER_COLOR));
        panel.add(toolToAdd, BorderLayout.CENTER);
        super.addComponentToCenter(panel);
    }

    public void addComponentToRightWithBorder(JComponent toolToAdd) {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setOpaque(false);
        panel.setBorder(BorderFactory.createMatteBorder(0,0,0,1,
                MacButtonFactory.GRADIENT_BUTTON_BORDER_COLOR));
        panel.add(toolToAdd, BorderLayout.CENTER);
        super.addComponentToRight(panel);
    }

    private void createAndInstallBackgroundPainter() {
        setBackgroundPainter
	    (PlatformUtils.isLeopard()
	     ? createLeopardPainter() : createDefaultPainter());
    }

    private void createAndInstallBorder() {
        FocusStateMatteBorder outterBorder = new FocusStateMatteBorder(1, 0, 0, 0,
                MacColorUtils.getTexturedWindowToolbarBorderFocusedColor(),
                MacColorUtils.getTexturedWindowToolbarBorderUnfocusedColor(),
                getComponent());
        Border innerBorder = BorderFactory.createMatteBorder(1, 0, 0, 0, getBorderHighlightColor());
        Border lineBorders = BorderFactory.createCompoundBorder(outterBorder, innerBorder);

        // TODO determine if there is a good standard for this. there doesn't seem to be any
        // TODO consistent value used by Apple.
        // left and right edge padding.
        int padding = 5;
        getComponent().setBorder(
                BorderFactory.createCompoundBorder(lineBorders,
                        BorderFactory.createEmptyBorder(0, padding, 0, padding)));
    }

    private static Painter<Component> createDefaultPainter() {
        Painter<Component> focusedPainter = new GradientPainter(
                ACTIVE_TOP_COLOR, ACTIVE_BOTTOM_COLOR);
        Painter<Component> unfocusedPainter = new GradientPainter(
                INACTIVE_TOP_COLOR, INACTIVE_BOTTOM_COLOR);
        return new FocusStatePainter(focusedPainter, focusedPainter, unfocusedPainter);
    }

    private static Painter<Component> createLeopardPainter() {
        Painter<Component> focusedPainter = new GradientPainter(
                LEOPARD_ACTIVE_TOP_COLOR, LEOPARD_ACTIVE_BOTTOM_COLOR);
        Painter<Component> unfocusedPainter = new GradientPainter(
                LEOPARD_INACTIVE_TOP_COLOR, LEOPARD_INACTIVE_BOTTOM_COLOR);
        return new FocusStatePainter(focusedPainter, focusedPainter, unfocusedPainter);
    }

    private static Color getBorderHighlightColor() {
        return PlatformUtils.isLeopard() ? LEOPARD_BORDER_HIGHLIGHT_COLOR : BORDER_HIGHLIGHT_COLOR;
    }
}
