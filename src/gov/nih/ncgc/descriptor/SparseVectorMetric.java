// $Id$

package gov.nih.ncgc.descriptor;

import java.util.Map;

public interface SparseVectorMetric {
    double compute (Map<String, Integer> v1, Map<String, Integer> v2);
}
