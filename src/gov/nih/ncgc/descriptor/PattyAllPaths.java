// $Id$

package gov.nih.ncgc.descriptor;

import chemaxon.struc.Molecule;
import chemaxon.struc.MolAtom;
import chemaxon.struc.MolBond;

import gov.nih.ncgc.util.MolPath;
import static gov.nih.ncgc.descriptor.PattyTyper.Type;

public class PattyAllPaths extends MolPath.AllPaths 
    implements SparseDescriptor, MolPath.Annotator {

    private Type[] pattyTypes;
    public PattyAllPaths (Molecule mol) {
	super (mol);
	pattyTypes = PattyTyper.getInstance().getTypes(mol);
    }

    @Override
    public String toString (MolBond[] path) {
	return MolPath.toString(path, this);
    }

    public String getLabel (MolAtom a) {
	int index = getMolecule().indexOf(a);
	return pattyTypes[index].toString();
    }
    public String getLabel (MolBond b) { return ""; }
}
