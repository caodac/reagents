// $Id: AtomPair.java 3498 2009-10-29 15:59:45Z nguyenda $
// compute atom pair (AP) descriptor type

package gov.nih.ncgc.descriptor;

import java.util.Map;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.Vector;
import java.util.Set;
import java.util.TreeSet;
import java.text.MessageFormat;

import chemaxon.struc.*;
import chemaxon.formats.*;
import chemaxon.util.MolHandler;

public class AtomPair implements SparseDescriptor {
    protected Map<String, Integer> sparseVector = 
	new TreeMap<String, Integer>();

    protected AtomPair () {}
    public AtomPair (Molecule g) {
	this (g, -1);
    }

    public AtomPair (Molecule g, int maxPath) {
	MolAtom atoms[] = g.getAtomArray();

	// compute atom property and pack it into an integer
	int[] codebook = encode (atoms);
	
	int [][]shortestPath = allShortestPaths (g);
	for (int i = 0; i < shortestPath.length; ++i) {
	    for (int j = i+1; j < shortestPath[i].length; ++j) {
		int length = shortestPath[i][j];
		if (maxPath < 0 || length <= maxPath) {
		    String path = decode (Math.max(codebook[i], codebook[j]))
			+ "_" + length  + "_"
			+ decode (Math.min(codebook[i], codebook[j]));
		    Integer count = sparseVector.get(path);
		    sparseVector.put(path, count != null ? count + 1 : 1);
		}
	    }
	}
    }

    public Map<String, Integer> getSparseVector () { return sparseVector; }

    public static String decode (int feature) {
	int atype = (feature >> 16) & 0x00ff;
	int pi = (feature & 0x0000ffff) >> 8;
	int nb = (feature & 0x000000ff);
	String name = (atype < 0xff ? MolAtom.symbolOf(atype) : "X");
	return name + pi + nb;
    }
    public static int encode (MolAtom atom) {
	int atype = 0xff;
	switch (atom.getAtno()) {
	case 6:
	case 7:
	case 8:
	case 9:
	case 15:
	case 16:
	case 17:
	case 35:
	case 53:
	    atype = atom.getAtno();
	    break;
	}
	int pi = numPiElectrons (atom);
	int nb = atom.getBondCount();

	return (atype << 16) | (pi << 8) | nb;
    }

    public static int[] encode (MolAtom[] atoms) {
	int vec[] = new int[atoms.length];
	for (int i = 0; i < atoms.length; ++i) {
	    vec[i] = encode (atoms[i]);
	}
	return vec;
    }

    public static int numPiElectrons (MolAtom atom) {
	if (atom.hasAromaticBond()) return 1;
	int valence = 0, order = atom.getBondCount();
	for (int i = 0; i < order; ++i) {
	    valence += atom.getBond(i).getType();
	}
	return valence - order;
    }

    public static int[][] allShortestPaths (Molecule g) {
	int atomCount = g.getAtomCount();
	int[][] shortestPaths = new int[atomCount][atomCount];
	int[][] tab = g.createBHtab();

	for (int i = 0; i < atomCount; ++i) {
	    shortestPaths[i][i] = 0;
	    for (int j = i+1; j < atomCount; ++j) {
		shortestPaths[j][i] = shortestPaths[i][j] 
		    = tab[i][j] < 0 ? atomCount : 1;
	    }
	}
	tab = null;

	/* Floyd's all-pairs shortest path algorithm */
	for (int k = 0; k < atomCount; ++k)
	    for (int i = 0; i < atomCount; ++i) 
		for (int j = 0; j < atomCount; ++j)
		    shortestPaths[i][j] = Math.min
			(shortestPaths[i][j], 
			 shortestPaths[i][k] + shortestPaths[k][j]);
			
	/*
	for (int i = 0; i < atomCount; ++i) {
	    for (int j = i+1; j < atomCount; ++j) {
		System.out.println
		    ("d("+(i+1)+","+(j+1)+") = " + shortestPaths[i][j]);
	    }
	}
	*/
	    
	return shortestPaths;
    }

    public static double cityBlockDistance 
	(Molecule g1, Molecule g2) {
	return cityBlockDistance 
	    (new AtomPair (g1).getSparseVector(),
	     new AtomPair (g2).getSparseVector());
    }

    public static double cityBlockDistance (Map<String, Integer> f1, 
					    Map<String, Integer> f2) {
	double d = 0.;
	for (Map.Entry<String, Integer> e : f1.entrySet()) {
	    Integer v2 = f2.get(e.getKey());
	    if (v2 != null) {
		// atom pair that's shared between two feature vectors
		d += Math.abs(e.getValue() - v2);
	    }
	    else {
		// this feature vector isn't in f2
		d += e.getValue();
	    }
	}

	// now count atom pairs in f2 that aren't in f1
	for (Map.Entry<String, Integer> e : f2.entrySet()) {
	    if (!f1.containsKey(e.getKey())) {
		d += e.getValue();
	    }
	}

	return d;
    }

    public static double similarity (Map<String, Integer> f1,
				     Map<String, Integer> f2) {
	double s = 0.;
	int f1count = 0;
	for (Map.Entry<String, Integer> e : f1.entrySet()) {
	    Integer v2 = f2.get(e.getKey());
	    if (v2 != null) {
		s += Math.min(e.getValue(), v2);
	    }
	    f1count += e.getValue();
	}
	int f2count = 0;
	for (Integer e : f2.values()) {
	    f2count += e;
	}
	s *= 2./(f1count + f2count);
	return s;
    }

    public static double similarity (Molecule g1, Molecule g2) {
	return similarity (new AtomPair(g1).getSparseVector(),
			   new AtomPair(g2).getSparseVector());
    }


    public static void main (String argv[]) throws Exception {
	if (argv.length < 1) {
	    System.out.println("AtomPair FILES...");
	    System.exit(1);
	}

	Class clazz = AtomPair.class;
	if (System.getProperty("ap.driver") != null) {
	    clazz = Class.forName(System.getProperty("ap.driver"));
	}

	for (int i = 0; i < argv.length; ++i) {
	    MolImporter importer = new MolImporter (argv[i]);
	    for (Molecule mol; (mol = importer.read()) != null; ) {
		mol.hydrogenize(false);
		mol.aromatize();
		mol.calcHybridization();

		AtomPair ap = (AtomPair)clazz.getConstructor
		    (Molecule.class).newInstance(mol);

		Map<String, Integer> sv = ap.getSparseVector();
		System.out.print(mol.getName());
		for (Map.Entry<String, Integer> e : sv.entrySet()) {
		    System.out.print(" " + e.getKey() + ":" + e.getValue());
		}
		System.out.println();
	    }
	    importer.close();
	}
    }
}
