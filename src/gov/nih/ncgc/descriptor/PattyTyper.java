// $Id: PattyTyper.java 2461 2009-03-01 23:12:12Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.io.InputStream;
import java.io.IOException;
import chemaxon.formats.MolImporter;
import chemaxon.struc.Molecule;

public class PattyTyper extends AtomTyper {
    public enum Type { 
	UNK,
	CAT, 
	    ANI, 
	    POL, 
	    DON, 
	    ACC, 
	    HYD, 
	    OTH;

	public static Type getInstance (String t) {
	    for (Type typ : Type.values()) {
		if (typ.toString().equals(t)) {
		    return typ;
		}
	    }
	    return UNK;
	}
	public static Type getInstance (int ord) {
	    for (Type typ : Type.values()) {
		if (ord == typ.ordinal()) {
		    return typ;
		}
	    }
	    return UNK;
	}
    }
    
    private static PattyTyper instance = null;
    private PattyTyper (InputStream is) throws IOException {
	super (is);
    }

    public static synchronized PattyTyper getInstance () {
	if (instance == null) {
	    try {
		instance = new PattyTyper 
		    (PattyTyper.class
		     .getResourceAsStream("resources/patty.rules"));
	    }
	    catch (Exception ex) {
		ex.printStackTrace();
	    }
	}
	return instance;
    }

    public Type[] getTypes (Molecule mol) {
	String[] types = apply (mol);
	Type[] patty = new Type[types.length];
	for (int i = 0; i < types.length; ++i) {
	    patty[i] = Type.getInstance(types[i]);
	}
	return patty;
    }

    public static void main (String[] argv) throws Exception {
	if (argv.length < 1) {
	    System.out.println("usage: PattyTyper FILES...");
	    System.exit(1);
	}
	
	AtomTyper typer = PattyTyper.getInstance();
	for (int i = 0; i < argv.length; ++i) {
	    MolImporter mi = new MolImporter (argv[i]);
	    for (Molecule mol = new Molecule (); mi.read(mol); ) {
		System.out.println(mol.getName());
		String[] types = typer.apply(mol);
		for (int j = 0; j < types.length; ++j) {
		    PattyTyper.Type type =
			PattyTyper.Type.getInstance(types[j]);
		    System.out.printf(" Atom %1$2d: %2$s %3$d\n", j+1, type,
				      type.ordinal());
		}
	    }
	    mi.close();
	}
    }
}
