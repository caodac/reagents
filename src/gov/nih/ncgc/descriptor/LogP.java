// $Id: LogP.java 2461 2009-03-01 23:12:12Z nguyenda $

package gov.nih.ncgc.descriptor;

import java.io.InputStream;
import java.io.IOException;

public class LogP extends GroupContrib {
    private static LogP instance = null;
    
    private LogP (InputStream is) throws IOException {
	super (is);
    }

    public synchronized static GroupContrib getInstance () {
	if (instance == null) {
	    try {
		instance = new LogP 
		    (LogP.class.getResourceAsStream("resources/logP.rules"));
	    }
	    catch (IOException ex) {
		ex.printStackTrace();
	    }
	}
	return instance;
    }
}
